﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;

/// <summary>
/// Various states of the player menu:
/// </summary>
public enum PlayerMenuState {
	/// <summary>
	/// The player can chose a faction in this menu.
	/// </summary>
	FactionMenu,
	/// <summary>
	/// The play must wait for other players to get ready.
	/// </summary>
	StandbyMenu,
	/// <summary>
	/// This is the ingame menu when pausing the game.
	/// </summary>
	IngameMenu,
	/// <summary>
	/// No menus, show player HUD instead.
	/// </summary>
	Ingame,
}


[System.Serializable]
public class PlayerBriefingSequence {
	public string commander = "Commander";
	[System.NonSerialized]
	public int briefingStep = 0;
	public string[] briefing = new string[1];
	private float lastStep = 0.0f;
	public float stepDuration = 1.5f;

	public void StartBriefing (PlayerInterface pi) {
		briefingStep = 0;
		lastStep = Time.time;
		pi.briefing.parentObj.SetActive(true);
		pi.briefing.commanderTitle.text = commander;
		pi.briefing.briefingText.text = briefing[0];
	}

	public void Update (PlayerInterface pi) {
		if (Time.time > lastStep + stepDuration) {
			briefingStep++;
			if (briefingStep < briefing.Length) {
				pi.briefing.briefingText.text = briefing[briefingStep];
			}
			else {
				pi.briefing.parentObj.SetActive(false);
			}
			lastStep = Time.time;
		}
	}
}

/// <summary>
/// A single human player's menu, contains links to HUD and player controller instances.
/// </summary>
public class PlayerMenu : MonoBehaviour {

	private PlayerMenuState menuState = PlayerMenuState.FactionMenu;
	/// <summary>
	/// The faction this player has joined.
	/// </summary>
	private PlayerFaction faction = PlayerFaction.Spectator;
	/// <summary>
	/// The index of the player, used for splitscreen position and controller source input.
	/// </summary>
	private int playerIndex = 1;

	/// <summary>
	/// The player controller responsible mostly for ingame controls.
	/// </summary>
	public PlayerController control = null;
	private Camera cam = null;

	// UI GROUPS:

	public GameObject factionMenu = null;
	public GameObject standbyMenu = null;
//	public GameObject shipMenu = null;
	public GameObject ingameMenu = null;
	public GameObject ingameHUD = null;

	public PlayerBriefingSequence briefing = new PlayerBriefingSequence();

	private float lastRadarPing = -1.0f;

	// PREFAB VEHICLES:

	/// <summary>
	/// The robot faction's spaceship.
	/// </summary>
	public Spaceship robotShip = null;
	/// <summary>
	/// The insect faction's spaceship.
	/// </summary>
	public Spaceship insectShip = null;


	// INITIALISATION:

	// Use this for initialization
	void Start () {
		SetMenuState(PlayerMenuState.FactionMenu);
		Object.DontDestroyOnLoad(gameObject);
		Initialise();
	}

	private void Initialise () {
		cam = control.gameObject.GetComponent<Camera>();
		GameMaster.players[playerIndex - 1] = control;
	}

	public void SetPlayerIndex (int newIndex = 1) {
		playerIndex = Mathf.Clamp(newIndex, 1, 4);
		Initialise();
		ConfigureSplitscreen();
	}

	public void ConfigureSplitscreen () {
		float playerCount = (float)GameMaster.playerCount;
		float index = (float)playerIndex;

		// DISPLACE UI CANVAS:	(full screen space)
		float uiPosX = 0.0f;
		float uiPosY = 0.0f;
		// Two players only? Horizontal displacement;
		if (playerCount == 2 && index == 2) {
			uiPosX = 0.5f;
		}
		// Three or four players? Horizontal and vertical displacement:
		else if (playerCount >= 3) {
			if (index == 1) {
				uiPosY = 0.5f;
			}
			if (index == 2) {
				uiPosX = 0.5f;
				uiPosY = 0.5f;
			}
			else if (index == 4) {
				uiPosX = 0.5f;
			}
		}

		// CAMERA SCREEN SIZE:
		float uiSizeX = (playerCount > 1) ? 0.5f : 1.0f;
		float uiSizeY = (playerCount > 2) ? 0.5f : 1.0f;

		// GET FINAL SCREEN RECT:
		cam.rect = new Rect(uiPosX, uiPosY, uiSizeX, uiSizeY);
	}

	/// <summary>
	/// State machine for the different menu states of the player UI and menu.
	/// </summary>
	/// <param name="state">The new menu state for this player.</param>
	public void SetMenuState (PlayerMenuState state) {
		menuState = state;
		if (state == PlayerMenuState.FactionMenu) {
			factionMenu.SetActive(true);
		}
		else {
			factionMenu.SetActive(false);
		}
		if (state == PlayerMenuState.StandbyMenu) {
			standbyMenu.SetActive(true);
		}
		else {
			standbyMenu.SetActive(false);
		}
		if (state == PlayerMenuState.IngameMenu) {
			ingameMenu.SetActive(true);
		}
		else {
			ingameMenu.SetActive(false);
		}
		if (state == PlayerMenuState.Ingame) {
			ingameHUD.SetActive(true);
		}
		else {
			ingameHUD.SetActive(false);
		}
	}

	void OnLevelWasLoaded (int level) {
		Debug.Log("Level " + level + " loaded; Starting game session!");
		if (level == 0) {
			Debug.Log("Game session ended. Terminating player controller " + playerIndex +  ".");
			Destroy(gameObject);
		}
		if (level == 2) {
			StartGameSession();
		}
	}

	// FACTION MENU:

	void Update () {
		// FACTION MENU:
		if (menuState == PlayerMenuState.FactionMenu) {
			if (control.device != null) {
				if (control.device.Action1.WasPressed) {
					UISelectRobots();
				}
				else if (control.device.Action2.WasPressed) {
					UIAutoAssign();
				}
					else if (control.device.Action4.WasPressed) {
					UISelectInsects();
				}
			}
		}
		// INGAME:
		else if (menuState == PlayerMenuState.Ingame) {
			if (control.device != null && control.device.MenuWasPressed) {
				GameMaster.TogglePauseGame(true);
			}
			briefing.Update(control.playerInterface);
		}
		// INGAME MENU:
		else if (control.device != null && menuState == PlayerMenuState.IngameMenu) {
			if (control.device.MenuWasPressed) {
				GameMaster.TogglePauseGame(false);
			}
		}
//		Debug.Log("Player menu state is now:   " + menuState.ToString());
	}

	/// <summary>
	/// UI button method to join robot faction:
	/// </summary>
	public void UISelectRobots () {
		JoinFaction(PlayerFaction.EvilRobots);
	}
	/// <summary>
	/// UI button method to join insect faction:
	/// </summary>
	public void UISelectInsects () {
		JoinFaction(PlayerFaction.SpaceBugs);
	}
	/// <summary>
	/// UI button method to automatically assign to faction:
	/// </summary>
	public void UIAutoAssign () {
		PlayerController[] players = GameMaster.playerControllers;
		int bugCount = 0;
		int botCount = 0;
		for (int i = 0; i<players.Length; i++) {
			if (players[i] != null) {
				if (players[i].faction == PlayerFaction.EvilRobots) {
					botCount++;
				}
				else if (players[i].faction == PlayerFaction.SpaceBugs) {
					bugCount++;
				}
			}
		}
		if (bugCount > botCount) {
			faction = PlayerFaction.EvilRobots;
		}
		else {
			faction = PlayerFaction.SpaceBugs;
		}
		JoinFaction(faction);
	}

	/// <summary>
	/// Make this player join a specific faction.
	/// </summary>
	/// <param name="newFaction">The faction the player is supposed to join.</param>
	private void JoinFaction (PlayerFaction newFaction = PlayerFaction.Spectator) {
		// Assign selected faction:
		faction = newFaction;
		control.SetFaction(newFaction);

		SetMenuState(PlayerMenuState.StandbyMenu);
		playerIndex = control.GetPlayerIndex();
		// Tell the game master this player is ready:
		GameMaster.playerReady[playerIndex - 1] = true;
		// If all players have confirmed they're ready, load level:
		if (GameMaster.CheckAllPlayersReady() == true) {
			SceneManager.LoadScene(2);
		}
		Image[] facButtons = control.playerInterface.joinFactions;
		if (faction == PlayerFaction.EvilRobots) {
			facButtons[0].color = new Color(1.0f, 1.0f, 1.0f, 0.25f);
			facButtons[2].color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		}
		else {
			facButtons[0].color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
			facButtons[2].color = new Color(1.0f, 1.0f, 1.0f, 0.25f);
		}
		facButtons[1].color = new Color(1.0f, 1.0f, 1.0f, 0.25f);
	}

	// GAME SESSION:

	/// <summary>
	/// Get the actual gameplay started and spawn vehicles:
	/// </summary>
	private void StartGameSession () {
		SetMenuState(PlayerMenuState.Ingame);
//		Debug.Log("Player is joining game session. Spawning ship:");
		control.InitSpawnPoint(faction);
		if (faction == PlayerFaction.EvilRobots) {
			control.SpawnVehicle(robotShip);
		}
		else if (faction == PlayerFaction.SpaceBugs) {
			control.SpawnVehicle(insectShip);
		}
		briefing.StartBriefing(control.playerInterface);
	}

	/// <summary>
	/// Updates the player's radar on ingame UI.
	/// This method is called by the controller itself, so no need to worry about that.
	/// </summary>
	public void UpdatePlayerRadar () {
		if (Time.time > lastRadarPing + 0.063f && control.vehicle != null) {
			PlayerRadarInterface radar = control.playerInterface.radarInterface;
			float radarRange = radar.radarRange;
			float radarScale = 100.0f / radarRange;
			float depthFactor = radarScale * 0.01f;

			string allyTag = "Player";
			string enemyTag = "Enemy";
			if (faction == PlayerFaction.SpaceBugs) {
				allyTag = "Enemy";
				enemyTag = "Player";
			}

			UpdatePlayerRadarGroup(allyTag, radar.allies, radarRange, radarScale, depthFactor);
			UpdatePlayerRadarGroup(enemyTag, radar.enemies, radarRange, radarScale, depthFactor);
			UpdatePlayerRadarGroup("ItemBox", radar.pickups, radarRange, radarScale, depthFactor);

			LevelMaster level = GameMaster.GetLevelMaster();
			if (level != null && control.vehicle != null) {
				Transform ship = control.vehicle.transform;

				Transform techBase = level.techBase.baseObject.transform;
				Vector3 techBaseDir = techBase.position - ship.position;
				UpdatePlayerRadarElement(techBase, techBaseDir, radar.techBase, radarScale, depthFactor);

				if (level.hiveBase.baseObject != null) {
					Transform hiveBase = level.hiveBase.baseObject.transform;
					Vector3 hiveBaseDir = hiveBase.position - ship.position;
					UpdatePlayerRadarElement(hiveBase, hiveBaseDir, radar.hiveBase, radarScale, depthFactor);
				}
				else {
					radar.hiveBase.gameObject.SetActive(false);
				}
			}

			lastRadarPing = Time.time;
		}
	}
	private void UpdatePlayerRadarGroup (string searchTag, Image[] radarIcons, float radarRange,
		float radarScale, float depthFactor) {
		int i = 0;
		GameObject[] contacts = GameObject.FindGameObjectsWithTag(searchTag);

		if (contacts != null) {
			int allyCounter = 0;
			int maxAllyCount = radarIcons.Length;
			Transform[] allyContacts = new Transform[maxAllyCount];
			float[] allyDists = new float[maxAllyCount];
			Vector3[] contactDirs = new Vector3[maxAllyCount];
			
			for (i = 0; i<contacts.Length; i++) {
				if (contacts[i].gameObject != control.vehicle.gameObject) {
					Vector3 allyDir = contacts[i].transform.position - control.vehicle.transform.position;
					float allyDist = allyDir.magnitude;
					if (searchTag != "ItemBox" || allyDist < radarRange) {
						if (allyCounter < maxAllyCount) {
							allyContacts[allyCounter] = contacts[i].transform;
							allyDists[allyCounter] = allyDist;
							contactDirs[allyCounter] = allyDir;
						}
						else {
							for (int a = 0; a<maxAllyCount; a++) {
								if (allyDist < allyDists[a]) {
									allyContacts[a] = contacts[i].transform;
									allyDists[a] = allyDist;
									contactDirs[a] = allyDir;
									break;
								}
							}
						}
						allyCounter++;
					}
				}
			}
			for (i = 0; i<maxAllyCount; i++) {
				if (allyContacts[i] != null) {
					radarIcons[i].gameObject.SetActive(true);
					UpdatePlayerRadarElement(contacts[i].transform, contactDirs[i], radarIcons[i], radarScale, depthFactor);
				}
				else {
					// No contact in this slot, hide UI icon:
					radarIcons[i].gameObject.SetActive(false);
				}
			}
		}
	}

	private void UpdatePlayerRadarElement (Transform contact, Vector3 contactDir, Image radarIcon,
		float dotRescale, float depthFactor) {
		// There is a clear contact for this slot, show it on UI:
		Color contactColor = Color.white;
		if (((contact.GetComponent<Spaceship>() &&
			contact.GetComponent<Spaceship>().control.isCarryingMine == true) ||
				(contact.GetComponent<Controller>() &&
					contact.GetComponent<Controller>().isCarryingMine == true))) {
			contactColor = Color.red;
		}

		// Get contact's coordinates in radar space:
		float allyX = Vector3.Dot(contactDir, control.vehicle.transform.right) * dotRescale;
		float allyY = Vector3.Dot(contactDir, control.vehicle.transform.forward) * dotRescale;
		float allyDepth = Vector3.Dot(contactDir, control.vehicle.transform.up) * dotRescale;
		// Modulate icon opacity based on vertical distance:
		contactColor.a = 1.0f - Mathf.Abs(allyDepth) * depthFactor;
		radarIcon.color = contactColor;
		// Reposition icon on radar UI:
		Vector3 normalizedPos = Vector3.ClampMagnitude(new Vector3(allyX, allyY, 0.0f), 95.0f);
		radarIcon.transform.localPosition = normalizedPos;
	}

}
