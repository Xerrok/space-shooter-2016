﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using InControl;

public enum MainMenuState {
	Splash,
	Main,
	Tutorial,
	Credits,
	Lobby
}

[System.Serializable]
public class MainMenuSplashScreen {
	public GameObject parentObj = null;
	public Image[] splashScreens = new Image[2];
	[System.NonSerialized]
	public int splashID = 0;
	private float startTime = 0.0f;

	public void Start () {
		parentObj.SetActive(true);
		splashID = 0;
		for (int i = 0; i<splashScreens.Length; i++) {
			if (i != 0) {
				splashScreens[i].gameObject.SetActive(false);
			}
			else {
				splashScreens[0].gameObject.SetActive(true);
			}
		}
		startTime = Time.time;
	}
	public void Update (MainMenu main, InputDevice device) {
		if (Input.GetKeyDown("escape") || (device != null && device.Action1.WasPressed) || Time.time > startTime + 3.0f) {
			splashID++;
			if (splashID < splashScreens.Length) {
				if (splashID > 0) {
					splashScreens[splashID - 1].gameObject.SetActive(false);
				}
				splashScreens[splashID].gameObject.SetActive(true);
			}
			else {
				main.SetMenuState(MainMenuState.Main);
			}
			startTime = Time.time;
		}
	}
}
[System.Serializable]
public class MainMenuCreditScreen {
	public GameObject parentObj = null;
	private float scrollDist = 0.0f;
	public float scrollSpeed = 24.0f;
	public Transform[] scrollBits = new Transform[6];

	public void Start () {
		parentObj.SetActive(true);
		scrollDist = 0.0f;
		for (int i = 0; i<scrollBits.Length; i++) {
			scrollBits[i].localPosition = new Vector3(0.0f, -320.0f);
		}
	}
	public void Update (MainMenu main, InputDevice device) {
		scrollDist += Time.deltaTime * scrollSpeed;
		for (int i = 0; i<scrollBits.Length; i++) {
			float sbitPos = (scrollDist > i * 320.0f) ? scrollDist - i * 320.0f : -320.0f;
			scrollBits[i].localPosition = new Vector3(0.0f, sbitPos);
		}

		if (Input.GetKeyDown("escape") || (device != null && device.Action1.WasPressed) ||
			scrollDist > (3.0f + (float)scrollBits.Length) * 320.0f) {
			main.SetMenuState(MainMenuState.Main);
			// Reset scroll bit positions:	(no particular reason, but cleans shit up anyways...)
			for (int i = 0; i<scrollBits.Length; i++) {
				scrollBits[i].localPosition = new Vector3(0.0f, -320.0f);
			}
		}
	}
}


public class MainMenu : MonoBehaviour {

	private MainMenuState menuState = MainMenuState.Splash;

	public MainMenuSplashScreen splashScreens;
	public MainMenuCreditScreen creditScreens;

	/// <summary>
	/// The player's controller prefab.
	/// </summary>
	public GameObject playerPrefab = null;

	/// <summary>
	/// The main menu canvas' game object.
	/// </summary>
	public GameObject menuCanvas = null;
	public GameObject lobbyCanvas = null;
	public GameObject tutorialCanvas = null;

	public Image[] menuButtons = new Image[4];
	public Image[] lobbyButtons = new Image[4];

	public Image[] gamepadIcons = new Image[4];

	public MovieTexture tutorialClip = null;

	public AudioSource mainMenueTheme = null;

//	public InputManager inputManager = null;
	private List<InputDevice> devices = new List<InputDevice>();
	

	void Start () {
		SetMenuState(MainMenuState.Splash);
	}

	public void SetMenuState (MainMenuState state) {
		menuState = state;
		if (menuState == MainMenuState.Main) {
			menuCanvas.SetActive(true);
			mainMenueTheme.Play();
			EventSystem.current.SetSelectedGameObject(menuButtons[0].gameObject);
		}
		else {
			menuCanvas.SetActive(false);
		}
		if (menuState == MainMenuState.Lobby) {
			lobbyCanvas.SetActive(true);
			EventSystem.current.SetSelectedGameObject(lobbyButtons[0].gameObject);
			for (int l = 0; l<4; l++) {
				Color lobCol = lobbyButtons[l].color;
				if (l == 0 || l < devices.Count) {
					lobCol.a = 1.0f;
				}
				else {
					lobCol.a = 0.25f;
				}
				lobbyButtons[l].color = lobCol;
			}
		}
		else {
			lobbyCanvas.SetActive(false);
		}
		if (menuState == MainMenuState.Credits) {
			creditScreens.Start();
		}
		else {
			creditScreens.parentObj.SetActive(false);
		}
		if (menuState == MainMenuState.Tutorial) {
			tutorialCanvas.SetActive(true);
			tutorialClip.Play();
		}
		else {
			tutorialCanvas.SetActive(false);
		}
		if (menuState == MainMenuState.Splash) {
			splashScreens.Start();
		}
		else {
			splashScreens.parentObj.SetActive(false);
		}
	}

	// MAIN MENU:

	public void MainStartNewGame () {
		// Deactivate main menu UI:
		SetMenuState(MainMenuState.Lobby);
	}

	void Update () {
		if (menuState == MainMenuState.Credits) {
			creditScreens.Update(this, (devices.Count > 0 && devices[0] != null) ? devices[0] : null);
		}
		else if (menuState == MainMenuState.Splash) {
			splashScreens.Update(this, (devices.Count > 0 && devices[0] != null) ? devices[0] : null);
		}
		else if (menuState == MainMenuState.Tutorial) {
			if (Input.GetKeyDown(KeyCode.Escape) || Input.GetButtonDown("Cancel") || Input.GetButtonDown("Submit")) {
				SetMenuState(MainMenuState.Main);
				tutorialClip.Stop();
			}
		}
		else if (menuState == MainMenuState.Lobby) {
			if (Input.GetKeyDown(KeyCode.Escape) || Input.GetButtonDown("Cancel")) {
				UIReturnToMainMenu();
			}
		}

		InputDevice curDevice = InputManager.ActiveDevice;
		if (curDevice != null) {
			if (curDevice.Action1.WasPressed || curDevice.Action2.WasPressed ||
				curDevice.Action3.WasPressed || curDevice.Action4.WasPressed) {
				bool inUse = false;
				for (int i = 0; i<devices.Count; i++) {
					if (devices[i] != null && devices[i] == curDevice) {
						inUse = true;
						break;
					}
				}
				if (inUse == false) {
					devices.Add(curDevice);
					Color devCol = gamepadIcons[devices.Count - 1].color;
					devCol.a = 0.8f;
					gamepadIcons[devices.Count - 1].color = devCol;
				}
			}
		}
	}

	public void UIStartTutorial () {
		SetMenuState(MainMenuState.Tutorial);
	}
	public void UIEnterCreditScreen () {
		SetMenuState(MainMenuState.Credits);
	}
	public void UIExitGame () {
		Application.Quit();
	}

	// PLAYER LOBBY:

	public void UISetPlayers (int newPlayerCount) {
		if (newPlayerCount == 1 || newPlayerCount <= devices.Count) {
			GameMaster.SetPlayerCount(newPlayerCount);
			LobbyStartGame();
		}
	}
	public void UIReturnToMainMenu () {
		SetMenuState(MainMenuState.Main);
	}

	/// <summary>
	/// Starts the game with a set number of players.
	/// </summary>
	public void LobbyStartGame () {
		Debug.Log("Spawning player for " + GameMaster.playerCount + " users.");
		// Deactivate player lobby:
		menuCanvas.SetActive(false);
		lobbyCanvas.SetActive(false);
		// Disable menu's main camera:
		Camera.main.gameObject.SetActive(false);
		// Prepare an array to store all players in:
		GameMaster.playerControllers = new PlayerController[GameMaster.playerCount];
		// Spawn player controllers:
		for (int i = 0; i<GameMaster.playerCount; i++) {
			// Spawn the controller GO:
			GameObject playerGO = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity) as GameObject;
			PlayerController player = playerGO.GetComponentInChildren<PlayerController>();
			// Assign player index to controller:
			player.SetPlayerIndex(i + 1);
			// Add controller to array of active players:
			GameMaster.playerControllers[i] = player;
			// Assign input device to player:
			if (devices.Count > i && devices[i] != null) {
				player.AssignNewInputDevice(devices[i]);
			}
			else {
				Debug.Log("ERROR! Controller " + i + " not found!");
			}
		}
	}

}
