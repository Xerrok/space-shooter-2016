﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[System.Serializable]
public class LevelBaseHealth {
	public GameObject baseObject = null;
	public int baseHealth = 6;
}

public class LevelMaster : MonoBehaviour {

	public LevelBaseHealth techBase;
	public LevelBaseHealth hiveBase;

	public GameObject hiveAI = null;
	public GameObject techAI = null;

	// Use this for initialization
	void Start () {
		GameMaster.SetLevelMaster(this);

		SpawnAIEnemies();
	}

	void OnDrawGizmos () {
		Gizmos.color = new Color(1.0f, 0.92f, 0.016f, 0.25f);
		if (techBase.baseObject != null) {
			Gizmos.DrawWireSphere(techBase.baseObject.transform.position, 10.0f);
		}
		if (hiveBase.baseObject != null) {
			Gizmos.DrawWireSphere(hiveBase.baseObject.transform.position, 10.0f);
		}
	}

	public void SpawnAIEnemies () {
		int playerCount = GameMaster.playerCount;
		int aiCount = 8 - playerCount;
		int techCount = aiCount / 2;

		int techSpawned = 0;

		for (int i = playerCount - 1; i<8; i++) {
			GameObject spawnedAI = null;
			if (techSpawned <= techCount) {
				spawnedAI = Instantiate(techAI, new Vector3(150.0f, 0.0f, 1000.0f), Quaternion.identity) as GameObject;
				techSpawned++;
			}
			else {
				spawnedAI = Instantiate(hiveAI, new Vector3(150.0f, 0.0f, -1000.0f), Quaternion.identity) as GameObject;
			}
			GameMaster.players[i] = spawnedAI.GetComponent<Controller>();
		}
	}

	public void ApplyBaseDamage (PlayerFaction faction) {
		// Apply damage to base:
		if (faction == PlayerFaction.EvilRobots) {
			techBase.baseHealth--;
		}
		else if (faction == PlayerFaction.SpaceBugs) {
			hiveBase.baseHealth--;
		}

		// Update health on all players' UI elements:
		foreach (PlayerController pc in GameMaster.playerControllers) {
			if (pc != null) {
				pc.playerInterface.bsHealthBar.fillAmount = (float)techBase.baseHealth / 6.0f;
				pc.playerInterface.hiveHealthBar.fillAmount = (float)hiveBase.baseHealth / 6.0f;
			}
		}

		// Verify survival of tech faction's mpthership:
		if (techBase.baseHealth <= 0) {
			SetPlayerVictory(PlayerFaction.SpaceBugs);
		}
	}

	//ROBERT TAKE THIS METHOD
	public void SetPlayerVictory (PlayerFaction faction) {

		foreach (PlayerController pc in GameMaster.playerControllers){

			//show wintext in UI if succsess
			pc.playerInterface.winText.gameObject.SetActive(true);
			//show scoreboard in UI when game ends
			pc.playerInterface.scoreBoard.canvas.gameObject.SetActive(true);

			if (pc != null && faction == PlayerFaction.SpaceBugs){
				pc.playerInterface.winText.text = "BUGS WIN!";
			}
			else{
				pc.playerInterface.winText.text = "ROBOTS WIN!";
			}
		}

		Invoke("VictoryLoadLevel", 7.5f);
	}

	private void VictoryLoadLevel () {
		SceneManager.LoadScene(0);
	}

}
