﻿using UnityEngine;
using System.Collections;

public static class GameMaster {

	public static int playerCount = 1;
	public static bool[] playerReady = new bool[1];
	private static LevelMaster levelMaster = null;

	/// <summary>
	/// Array containing all active player's controllers.
	/// </summary>
	public static PlayerController[] playerControllers = new PlayerController[1];
	public static Controller[] players = new Controller[8];

	public static void SetPlayerCount (int newPlayerCount) {
		playerCount = Mathf.Min(newPlayerCount, 4);
		playerReady = new bool[playerCount];
	}

	/// <summary>
	/// Checks wether all players are ready and wether the game may begin.
	/// </summary>
	/// <returns><c>true</c>, if all players are ready, <c>false</c> otherwise.</returns>
	public static bool CheckAllPlayersReady () {
		bool ready = true;
		for (int i = 0; i<playerCount; i++) {
			if (playerReady[i] == false) {
				ready = false;
				break;
			}
		}
		if (ready == true) {
			Debug.Log("All Players ready!");
		}
		return ready;
	}

	public static void SetLevelMaster (LevelMaster level) {
		levelMaster = level;
	}
	public static LevelMaster GetLevelMaster () {
		return (levelMaster != null) ? levelMaster : null;
	}

	public static void ReportPlayerKill (PlayerKill death) {
		if (death.source != null) {
			if (death.source.GetComponent<Vehicle>()) {
				death.source.GetComponent<Vehicle>().control.PlayersKilled(1);
			}
			else if (death.source.GetComponent<SpaceshipAI>()) {
				death.source.GetComponent<SpaceshipAI>().PlayersKilled(1);
			}
		}

		death.killedPlayer.PlayerDied();

		for (int i = 0; i<playerControllers.Length; i++) {
			if (playerControllers[i] != null) {
//				Debug.Log("UPDATING PLAYER'S SCOREBOARD!  " + i);
				playerControllers[i].playerInterface.scoreBoard.UpdateScoreBoard();
			}
		}
	}

	public static void TogglePauseGame (bool pauseGame = true) {
		PlayerMenuState newMenuState = PlayerMenuState.IngameMenu;
		if (pauseGame == false) {
			newMenuState = PlayerMenuState.Ingame;
		}
		for (int i = 0; i<playerCount; i++) {
			if (playerControllers[i] != null) {
				playerControllers[i].playerMenu.SetMenuState(newMenuState);
			}
		}
	}

}

public class PlayerKill {
	public Controller killedPlayer = null;
	public GameObject source = null;

	public PlayerKill (Controller killed, GameObject deathCause = null) {
		killedPlayer = killed;
		source = (deathCause != null) ? deathCause : null;
	}

}

public struct KillDeathRatio {
	public int kills;
	public int deaths;

	public float GetKillDeathRatio () {
		return (float)kills / Mathf.Min(1.0f, (float)deaths);
	}

	public override string ToString () {
		return (kills + " / " + deaths);
	}
}