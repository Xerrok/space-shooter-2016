using UnityEngine;
using System.Collections;

public class ItemRotator : MonoBehaviour 
{
	private Vector3 itemPos;
	private float	oldHoverSpeed;
	public	float	hoverSpeed;
	public	float	hoverHeight;
	public	float	rotateSpeed;

	// Use this for initialization
	void Start () 
	{
		itemPos 		= transform.position;
		oldHoverSpeed	= hoverSpeed;

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (transform.position.y < itemPos.y)
		{
			hoverSpeed = oldHoverSpeed;
		}

		else if (transform.position.y > itemPos.y + hoverHeight)
		{
			hoverSpeed = -hoverSpeed;
		}

		transform.Translate (new Vector3 (0, hoverSpeed, 0) * Time.deltaTime);

		transform.Rotate (new Vector3 (0, rotateSpeed, 0) * Time.deltaTime);
	}
}
