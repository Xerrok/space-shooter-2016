﻿using UnityEngine;
using System.Collections;

public class ItemSpawner : MonoBehaviour
{
    public  float           respawnTimer;

    private int[]           amountOfInvisibleUp;
    private float[]         timer;
    private float[] timer_2;
    private float[] timer_3;
    private GameObject[]    invisibleUpgrades;
    private GameObject[] MineBoxItem;
    private GameObject[] MissileBoxItem;


	// Use this for initialization
	void Start ()
    {
        invisibleUpgrades = GameObject.FindGameObjectsWithTag("InvisibleUpgrade");
        MineBoxItem = GameObject.FindGameObjectsWithTag("MineItemBox");
        MissileBoxItem = GameObject.FindGameObjectsWithTag("MissileItemBox");
		timer = new float[invisibleUpgrades.Length];
        timer_2 = new float[MineBoxItem.Length];
        timer_3 = new float[MissileBoxItem.Length];
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (invisibleUpgrades != null && timer != null)
        {
			for (int i = 0; i < timer.Length; i++)
            {
				if (invisibleUpgrades[i].activeSelf == false)
                {
                    timer[i] += Time.deltaTime;

                    if (timer[i] > respawnTimer)
                    {
                        invisibleUpgrades[i].SetActive(true);
                        timer[i] = 0;
                    }
                }
			}
        }

		if (MineBoxItem != null && timer_2 != null)
        {
            for (int i = 0; i < timer_2.Length; i++)
            {
                if (MineBoxItem[i].activeSelf == false)
                {
                    timer_2[i] += Time.deltaTime;

                    if (timer_2[i] > respawnTimer)
                    {
                        MineBoxItem[i].SetActive(true);
                        timer_2[i] = 0;
                    }
                }
            }
        }

        if (MissileBoxItem != null)
        {
            for (int i = 0; i < timer_2.Length; i++)
            {
                if (MissileBoxItem[i].activeSelf == false)
                {
                    timer_3[i] += Time.deltaTime;

                    if (timer_3[i] > respawnTimer)
                    {
                        MissileBoxItem[i].SetActive(true);
                        timer_3[i] = 0;
                    }
                }
            }
        }
    }

}
