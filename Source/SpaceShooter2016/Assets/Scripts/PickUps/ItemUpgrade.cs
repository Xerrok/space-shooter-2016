﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemUpgrade : MonoBehaviour
{
    public Material mat_normalUpgrade;
    public Material mat_normalSmoke;
    public Material mat_invisibleUpgrade;
    public Material mat_invisibleSmoke;
    public float invisibleDuration;
    public float normalLifeTime;
    public float transparencyLifeTime;
    private bool isInvisible;
	private float timer;

	public float cooldownBoost = 0.0f;
	private float resetBoost = 0.0f;
	private bool boostCollected = false;

    public float cooldownInvisible = 1.0f;
    private float resetInvisible = 1.0f;
    private Renderer[] amountOfPlayerParts;
    private ParticleSystem[] spaceshipSmoke;

    private bool missileCollected =false;
    private bool mineCollected = false;
    private bool invisibleUpgradeCollected = false;

    public bool shieldCollected = false;
    private float leftShieldTime = 0.0f;
    public float maxShieldTime = 12.0f;

    private Vehicle vehicle = null;
    private PlayerInterface playerInterface = null;
	private PlayerController playerControl = null;


    void Start()
    {
        vehicle = GetComponent<Vehicle>();
        playerInterface = (vehicle.control as PlayerController).playerInterface;
        spaceshipSmoke = GetComponentsInChildren<ParticleSystem>();
        amountOfPlayerParts = GetComponentsInChildren<Renderer>();

        playerInterface.boostIcon.gameObject.SetActive(false);
        playerInterface.mineIcon.gameObject.SetActive(false);
        playerInterface.rocketIcon.gameObject.SetActive(false);
        playerInterface.shieldIcon.gameObject.SetActive(false);
        playerInterface.ammoMines.gameObject.SetActive(false);
        playerInterface.ammoMissiles.gameObject.SetActive(false);
		playerInterface.boostBarFull.gameObject.SetActive(false);

		resetInvisible = cooldownInvisible;        
		resetBoost = cooldownBoost;
		(vehicle as Spaceship).maxFuel = 0;
		(vehicle as Spaceship).maxFuel = (vehicle as Spaceship).boostFuel;
        leftShieldTime = 0.0f;

		if(vehicle !=null)
		{
			playerControl = vehicle.control as PlayerController;
		}
			
    }

    /// <summary>
    /// Fixeds the update.
    /// </summary>
    void FixedUpdate(){
        if (isInvisible){
            timer += Time.deltaTime;

            if (timer >= invisibleDuration){
                InvisibleUpgradeOff();
            }
        }
       
        if(mineCollected ==true)
        {
            playerInterface.ammoMines.gameObject.SetActive(true);
            playerInterface.ammoMines.text = ("" + GetComponent<Spaceship>().weaponSystem.mine.ammo);

            if (GetComponent<Spaceship>().weaponSystem.mine.ammo == 0)
            {
                playerInterface.ammoMines.gameObject.SetActive(false);
                playerInterface.mineIcon.gameObject.SetActive(false);
                mineCollected = false;
            }
        }
        if(missileCollected ==true)
        {
            playerInterface.ammoMissiles.gameObject.SetActive(true);
            playerInterface.ammoMissiles.text = ("" + GetComponent<Spaceship>().weaponSystem.rocket.ammo);

            if (GetComponent<Spaceship>().weaponSystem.rocket.ammo == 0)
            {
                playerInterface.ammoMissiles.gameObject.SetActive(false);
                playerInterface.rocketIcon.gameObject.SetActive(false);
               missileCollected = false;
            }
        }

        if(shieldCollected ==true)
        {
            leftShieldTime -= Time.deltaTime;
            DamageCalc dmgcalc = transform.GetComponent<DamageCalc>();
			float fillAmountShield = leftShieldTime / maxShieldTime;
            (GetComponent<Vehicle>().control as PlayerController).playerInterface.shieldIconCooldown.fillAmount = fillAmountShield;
            if (leftShieldTime <= 0 || dmgcalc.leftShieldHitpoints<=0)
            {
                dmgcalc.maxShieldHitpoints = 0;
                dmgcalc.leftShieldHitpoints = dmgcalc.maxShieldHitpoints;
                
                playerInterface.shieldBarPlayer.gameObject.SetActive(false);
                playerInterface.shieldIcon.gameObject.SetActive(false);
                playerInterface.shieldIconCooldown.gameObject.SetActive(false);
                leftShieldTime = 0.0f;
				shieldCollected = false;


            }

        }

		if (shieldCollected == false) 
		{
			DamageCalc dmgcalc = transform.GetComponent<DamageCalc>();
			dmgcalc.maxShieldHitpoints = 0;
			dmgcalc.leftShieldHitpoints = dmgcalc.maxShieldHitpoints;


			playerInterface.shieldBarPlayer.gameObject.SetActive(false);
			playerInterface.shieldIcon.gameObject.SetActive(false);
			playerInterface.shieldIconCooldown.gameObject.SetActive(false);				
//			Debug.Log("disable shieldIcon");
			leftShieldTime = 0.0f;
		}
		if (boostCollected == true)
		{
			playerInterface.boostIcon.gameObject.SetActive(true);
			playerInterface.boostIconCooldown.gameObject.SetActive(true);
			playerInterface.boostBarFull.gameObject.SetActive (true);
			cooldownBoost -= Time.deltaTime; //cooldown BoostIcon
			float fillAmountBoost = cooldownBoost / resetBoost;
			float fillAmountFuel =(vehicle as Spaceship).boostFuel / (vehicle as Spaceship).maxFuel;
			(GetComponent<Vehicle>().control as PlayerController).playerInterface.boostIconCooldown.fillAmount = fillAmountBoost;
			(GetComponent<Vehicle>().control as PlayerController).playerInterface.boostBarFull.fillAmount = fillAmountFuel;


			if (cooldownBoost <= 0 && playerInterface != null)// || playerControl.currentBoostTime <= 0)
			{
				(vehicle as Spaceship).maxFuel= 0;
				(vehicle as Spaceship).boostFuel = (vehicle as Spaceship).maxFuel;

				playerInterface.boostIconCooldown.gameObject.SetActive(false);
				playerInterface.boostIcon.gameObject.SetActive(false);
				playerInterface.boostBarFull.gameObject.SetActive(false);
				cooldownBoost = resetBoost;
				boostCollected = false;
			}
		}

        if(invisibleUpgradeCollected ==true)
        {
            cooldownInvisible -= Time.deltaTime;//cooldown InvisibleIcon
            float fillAmountInvisible = cooldownInvisible / resetInvisible;
            (GetComponent<Vehicle>().control as PlayerController).playerInterface.invisibilityIconCooldown.fillAmount = fillAmountInvisible;
            if (cooldownInvisible <= 0 && playerInterface != null)
            {
                playerInterface.invisibilityIconCooldown.gameObject.SetActive(false);
                playerInterface.invisibleIcon.gameObject.SetActive(false);
                cooldownInvisible = resetInvisible;
                invisibleUpgradeCollected = false;	
            }

        }
		if (invisibleUpgradeCollected == false) {

			playerInterface.invisibilityIconCooldown.gameObject.SetActive(false);
			playerInterface.invisibleIcon.gameObject.SetActive(false);
			cooldownInvisible = 0.0f;
			//maxShieldTime = leftShieldTime;
//			Debug.Log("disable invisibleicon");
		}
    }

    void OnTriggerEnter(Collider other){
        if (other.gameObject.CompareTag("InvisibleUpgrade")){
            InvisibleUpgradeOn(other);
            other.gameObject.SetActive(false);
            //if the boost Upgrade collected by player then show it in the HUD
            if (playerInterface != null && playerInterface.invisibleIcon != null){
                playerInterface.invisibleIcon.gameObject.SetActive(true);
                playerInterface.invisibilityIconCooldown.gameObject.SetActive (true);
                invisibleUpgradeCollected = true;
				shieldCollected = false;
                cooldownInvisible = resetInvisible;
            }
        } else if (other.gameObject.CompareTag("MineItemBox")){
           
			GetComponent<Spaceship>().weaponSystem.mine.ammo = 5;
           
            other.gameObject.SetActive(false);
            //if the boost Upgrade collected by player then show it in the HUD
            if (playerInterface != null && playerInterface.mineIcon != null)
            {
                playerInterface.mineIcon.gameObject.SetActive (true);
                mineCollected = true;

                if (mineCollected ==true)
                {
                    playerInterface.rocketIcon.gameObject.SetActive(false);
                    GetComponent<Spaceship>().weaponSystem.rocket.ammo = 0;
                    playerInterface.ammoMissiles.gameObject.SetActive(false);
                    missileCollected = false;
                }
            }
        } else if (other.gameObject.CompareTag("MissileItemBox")){
           
			GetComponent<Spaceship>().weaponSystem.rocket.ammo =5;
            other.gameObject.SetActive(false);
            //if the boost Upgrade collected by player then show it in the HUD
            if (playerInterface != null && playerInterface.rocketIcon != null ){
               playerInterface.rocketIcon.gameObject.SetActive(true);
               missileCollected = true;

                if(missileCollected ==true){
                    playerInterface.mineIcon.gameObject.SetActive(false);
                    GetComponent<Spaceship>().weaponSystem.mine.ammo = 0;
                    playerInterface.ammoMines.gameObject.SetActive(false);
                    mineCollected = false;
                }
            }
        } else if (other.gameObject.CompareTag("ShieldUpgrade")){
            
			other.gameObject.SetActive(false);

            if (playerInterface != null && playerInterface.shieldIcon != null)
            {
                playerInterface.shieldIcon.gameObject.SetActive(true);
                playerInterface.shieldBarPlayer.gameObject.SetActive(true);
                DamageCalc dmgcalc = transform.GetComponent<DamageCalc>();
                dmgcalc.maxShieldHitpoints = 100;
                dmgcalc.leftShieldHitpoints = dmgcalc.maxShieldHitpoints;
                (GetComponent<Vehicle>().control as PlayerController).playerInterface.shieldBarPlayer.fillAmount = dmgcalc.maxShieldHitpoints;
                
                playerInterface.shieldIconCooldown.gameObject.SetActive(true);

				leftShieldTime = maxShieldTime;
                shieldCollected = true;
				invisibleUpgradeCollected = false;
            }
		} else if (other.gameObject.CompareTag("BoostItemBox")){

			other.gameObject.SetActive(false);

			if (playerInterface != null && playerInterface.boostIcon != null)
			{
				playerInterface.boostIcon.gameObject.SetActive(true);
				playerInterface.boostIconCooldown.gameObject.SetActive(true);
				playerInterface.boostBarFull.gameObject.SetActive(true);

				(vehicle as Spaceship).maxFuel = 100.0f;

				resetBoost = cooldownBoost;
				(vehicle as Spaceship).boostFuel = (vehicle as Spaceship).maxFuel;
				boostCollected = true;
			}
		}
    }

    void InvisibleUpgradeOn(Collider other)
    {
        timer = 0;
        isInvisible = true;

        if (amountOfPlayerParts != null)
        {
            foreach (Renderer parts in amountOfPlayerParts)
            {
                if (parts != null)
                {
                    if (parts.CompareTag("SpaceshipPart"))
                    {
                        parts.GetComponentInChildren<Renderer>().material = mat_invisibleUpgrade;
                    }

                    if (parts.CompareTag("SpaceshipParticle"))
                    {
                        parts.GetComponentInChildren<Renderer>().material = mat_invisibleSmoke;
                    }
                }
            }
        }

        if (spaceshipSmoke != null)
        {
            foreach (ParticleSystem ps in spaceshipSmoke)
            {
                if (ps != null)
                {
                    ps.startLifetime = transparencyLifeTime;
                }
            }
        }

        other.gameObject.SetActive(false);
    }

    void InvisibleUpgradeOff()
    {

        if (amountOfPlayerParts != null){
            foreach (Renderer parts in amountOfPlayerParts){
                if (parts != null){
                    if (parts.CompareTag("SpaceshipPart")){
                        parts.GetComponentInChildren<Renderer>().material = mat_normalUpgrade;
                    }

                    if (parts.CompareTag("SpaceshipParticle")){
                        parts.GetComponentInChildren<Renderer>().material = mat_normalSmoke;
                    }
                }
            }
        }


        if (spaceshipSmoke != null)
        {
            foreach (ParticleSystem ps in spaceshipSmoke)
            {
                if (ps != null)
                {
                    ps.startLifetime = normalLifeTime;
                }
            }
        }

        timer = 0;
        isInvisible = false;
    }
}
