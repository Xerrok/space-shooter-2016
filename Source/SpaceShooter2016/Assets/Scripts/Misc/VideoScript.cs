﻿using UnityEngine;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]

public class VideoScript : MonoBehaviour {

	public MovieTexture movie;

	// Use this for initialization
	void Start () {
        //ini the component for the movie
		GetComponent<RawImage>().texture = movie as MovieTexture;
		//plays the movie *yeah*
        movie.Play();
	}

	// Update is called once per frame
	void Update () {

    //pause the Clip
		if (Input.GetKeyDown(KeyCode.Space) && movie.isPlaying){
			movie.Pause();
		}
		else if (Input.GetKeyDown(KeyCode.Space) && !movie.isPlaying){
			movie.Play();
		}
	}
}