﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NetworkManager_Custom : NetworkManager {

	/// <summary>
	/// Startups the host.
	/// </summary>
	public  void StartupHost(){
		Debug.Log ("hosted");
		SetPort ();
		NetworkManager.singleton.StartHost ();
	}

	/// <summary>
	/// Joins the game.
	/// </summary>
	public void JoinGame(){
		SetIPAdress ();
		SetPort ();
		NetworkManager.singleton.StartClient ();
	}

	/// <summary>
	/// Sets the IP adress. 
	/// Checks if localhost string is null or not, 
	/// and helps to connect without knowledgs about the local ip
	/// </summary>
	void SetIPAdress(){
		string ipAdress = GameObject.Find ("InputFieldIPAdress").transform.FindChild ("Text").GetComponent<Text> ().text;
		if (ipAdress == ""|| ipAdress == null) {
			NetworkManager.singleton.networkAddress = ("localhost");
		} else {
			NetworkManager.singleton.networkAddress = ipAdress;
		}
	}

	/// <summary>
	/// Sets the port.
	/// </summary>
	void SetPort(){
		NetworkManager.singleton.networkPort = 7777;
	}

	/// <summary>
	/// Raises the level was loaded event.
	/// </summary>
	/// <param name="level">Level.</param>
	void OnLevelWasLoaded(int level){
		if (level == 0) {
			StartCoroutine(SetupMenueSceneButtons());
		} else {
			SetupOtherSceneButtons ();
		}
	}

	/// <summary>
	/// Setups the menue scene buttons.
	/// Allows the player to start/join the game.
	/// </summary>	
	 IEnumerator SetupMenueSceneButtons(){
		yield return new WaitForSeconds (0.3f);
		GameObject.Find ("HostButton").GetComponent<Button> ().onClick.RemoveAllListeners ();
		GameObject.Find ("HostButton").GetComponent<Button> ().onClick.AddListener (StartupHost);

		GameObject.Find ("JoinButton").GetComponent<Button> ().onClick.RemoveAllListeners ();
		GameObject.Find ("JoinButton").GetComponent<Button> ().onClick.AddListener (JoinGame);
	}

	/// <summary>
	/// Setups the other scene buttons (Ingame).
	/// Allows the Player to disconnect the game.
	/// </summary>
	public void SetupOtherSceneButtons(){
		Debug.Log (NetworkEventType.DisconnectEvent.ToString ());
		GameObject.Find ("ButtonDisconnect").GetComponent<Button> ().onClick.RemoveAllListeners ();
		GameObject.Find ("ButtonDisconnect").GetComponent<Button> ().onClick.AddListener (NetworkManager.singleton.StopHost);

	}
}
