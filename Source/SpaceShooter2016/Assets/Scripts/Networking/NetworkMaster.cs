﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

[AddComponentMenu("Scripts/Multiplayer/Network Master")]
public class NetworkMaster : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
		Object.DontDestroyOnLoad(transform.root.gameObject);
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
