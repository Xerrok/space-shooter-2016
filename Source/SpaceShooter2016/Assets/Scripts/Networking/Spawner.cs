﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

[AddComponentMenu("Scripts/Multiplayer/Spawner")]
public class MySpawner : NetworkBehaviour {

	public Spaceship playerPrefab;

	public void Spawn(Controller respawning){

		Spaceship player = (Spaceship)Instantiate (playerPrefab, transform.position, transform.rotation);
		respawning.AssignNewVehicle(player);
		
		NetworkServer.Spawn (player.gameObject);
	}

}
