﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Damage delivered by either a weapon, a collision or any other source of loss in physical integrity.
/// </summary>
[System.Serializable]
public struct Damage {
	/// <summary>
	/// The actual hitpoint damage we're talking about.
	/// </summary>
	public float damage;
	/// <summary>
	/// The guy who fired this shot (btw: Han shot first!).
	/// </summary>
	public GameObject source;
	/// <summary>
	/// Any debuff effects delivered by this damage event (think DoT, slow, invisibility, etc...).
	/// </summary>
//	public Debuffs debuff;

	/// <summary>
	/// Initializes a new instance of the <see cref="Damage"/> struct.
	/// </summary>
	/// <param name="dmg">Amount of hitpoints drained, aka the actual damage.</param>
	/// <param name="src">The source of the damage, aka who fired the shot; can be left blank.</param>
	public Damage (float dmg, GameObject src = null) {
		damage = dmg;
		source = (src != null) ? src : null;
		// Create a new debuff without adverse effects: (implement later if needed!)
//		debuff = new Debuffs(0.0f, 0.0f);
	}
}

[System.Serializable]
public struct Debuffs {
	/// <summary>
	/// Invert player controls on x-axis (pitch aka up/down).
	/// </summary>
	public bool invertedX;
	/// <summary>
	/// Invert player controls on y-axis (yaw aka left/right).
	/// </summary>
	public bool invertedY;

	/// <summary>
	/// A percentage of speed that is lost because of a debuff. [0≤x≤1]
	/// </summary>
	public float speedDrop;
	/// <summary>
	/// A percentage of rotation speed that is lost because of a debuff. [0≤x≤1]
	/// </summary>
	public float rotationSpeedDrop;

	/// <summary>
	/// Initializes a new specific instance of the <see cref="Debuffs"/> struct.
	/// </summary>
	/// <param name="newSpeedDrop">Percentage of speed lost through debuff.</param>
	/// <param name="newRotationDrop">Percentage of rotation speed lost through debuff.</param>
	public Debuffs (float newSpeedDrop = 0.5f, float newRotationDrop = 0.0f) {
		// Set control axis inversions:
		invertedX = false;
		invertedY = false;
		// Set numerical control drops:
		speedDrop = Mathf.Max(0.5f, newSpeedDrop);
		rotationSpeedDrop = Mathf.Max(0.5f, newRotationDrop);
	}
}
