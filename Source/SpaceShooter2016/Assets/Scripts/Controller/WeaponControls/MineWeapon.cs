﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[AddComponentMenu("Scripts/Weapons/Weapon Mine")]

public class MineWeapon : MonoBehaviour
{
    //private GameObject[] ItemMine;
    public int ammo = 1;
    public float placeMine = 1.0f; // Distanz beim Abwurf der Mine zwischen Raumschiff und Mine
    public float firerate = 1.0f;
    private float LastTimeThisMineWasUsed = 0.0f;
    
    public Rigidbody mineprojectile;


    public void PlaceMine()
    {
        Rigidbody cloneMine;
       
            if (Time.time > LastTimeThisMineWasUsed + firerate && ammo > 0)
            {
                cloneMine = Instantiate(mineprojectile, transform.position, transform.rotation) as Rigidbody;
                cloneMine.position = transform.TransformPoint(Vector3.back * placeMine);
                ammo--;
                cloneMine.GetComponent<MineProjectile>().LaunchMine(gameObject);
                LastTimeThisMineWasUsed = Time.time;
            }
        
       
    }
}
