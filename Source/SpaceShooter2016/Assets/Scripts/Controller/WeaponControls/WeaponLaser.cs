﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Scripts/Weapons/Weapon Laser")]
public class WeaponLaser : MonoBehaviour
{
    private BallisticMaster master;
    public Transform projectile;
    public float firerate = 0.1f;
	[System.NonSerialized]
    public float overheat = 0.0f;
    public float cooldown = 2.0f;
    public int maxHeat = 10;
    private float LastTimeThisWeaponWasUsed = 0.0f;
	private Vehicle vehicle;
	public float laserDamage = 10.0f;
	public float projectileSpeed = 200.0f;
	public float maxRange = 1000.0f;
	public float bulletSpread = 0.6f;
	private GameObject source = null;

	private PlayerController playerControl = null;


	// Use this for initialization
	void Start ()
    {
        master = GameObject.Find("_BallisticMaster").GetComponent<BallisticMaster>();
        if (vehicle != null) {
			source = vehicle.control.gameObject;
        }
        else {
        	source = transform.parent.gameObject;
        } 

		if (vehicle != null)
        {
            playerControl = vehicle.control as PlayerController;
        }
    }

	public void AssignNewVehicle (Vehicle newVehicle) {
		vehicle = newVehicle;
    }

    public void Update()
    {
		if (overheat > 0 && playerControl != null)
        {
            overheat -= Time.deltaTime * cooldown;
            //fillAmount in scence is 0 -> 1
            float fillAmount = overheat / maxHeat;
            playerControl.playerInterface.overheatBar.fillAmount = fillAmount;
        }
    }

    public void Fire(float fireSpeed = 1.0f)
    {
		if (Time.time > LastTimeThisWeaponWasUsed + firerate * (2.0f - fireSpeed) && overheat<maxHeat)
        {
			Vector3 direction = transform.forward;
			if (bulletSpread > 0.0f) {
				direction = Quaternion.Euler(Random.insideUnitSphere * bulletSpread) * transform.forward;
			}

			if (master == null) {
				master = GameObject.Find("_BallisticMaster").GetComponent<BallisticMaster>();
			}
			if (master != null) {
				master.CmdAddBallisticProjectile(transform.position, direction * projectileSpeed,
            	maxRange, source, laserDamage);
			}

            overheat++;
            LastTimeThisWeaponWasUsed = Time.time;
        }
    }
}
