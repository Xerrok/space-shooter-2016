﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Scripts/Weapons/Weapon Rocket")]
public class WeaponRocket : MonoBehaviour
{
    public int ammo = 10;
    public float firerate = 0.5f;
    private float LastTimeThisRocketWasUsed = -1.0f;
    public Rigidbody projectile;
    public Transform target = null;
	private Vehicle vehicle;

	private string targetTag = "Enemy";

	public float minLockingRange = 30.0f;
	public float maxLockingRange = 400.0f;
	public float maxLockingAngle = 25.0f;


	public void AssignNewVehicle (Vehicle newVehicle) {
		vehicle = newVehicle;
		if (vehicle != null) {
			targetTag = (vehicle.gameObject.tag == "Player") ? "Enemy" : "Player";
		}
    }

	public void Fire()
    {
        if (Time.time > LastTimeThisRocketWasUsed + firerate && ammo > 0)
        {
            Rigidbody clone;

            clone = Instantiate(projectile, transform.position, transform.rotation) as Rigidbody;
            ammo--;
			FindHomingTarget();
			clone.GetComponent<RocketProjectile>().LaunchRocket(target, vehicle.control.gameObject);
			Physics.IgnoreCollision(clone.GetComponent<Collider>(), vehicle.GetComponent<Collider>());
            LastTimeThisRocketWasUsed = Time.time;
            Debug.Log("Current missile ammo:" + ammo);
        }
	}


	private Transform FindHomingTarget () {
		GameObject[] targets = GameObject.FindGameObjectsWithTag(targetTag);

		if (targets != null) {
			float bestAngle = 360.0f;
			Transform bestTarget = null;

			for (int i = 0; i<targets.Length; i++) {
				Vector3 targetDir = targets[i].transform.position - transform.position;
				float targetDist = targetDir.magnitude;

				if (targetDist > minLockingRange && targetDist < maxLockingRange) {
					float targetAngle = Vector3.Angle(transform.forward, targetDir);
					if (targetAngle < bestAngle && targetAngle < maxLockingAngle) {
						bestTarget = targets[i].transform;
						bestAngle = targetAngle;
					}
				}

				if (bestTarget != null) {
					target = bestTarget;
					return target;
				}
			}
		}

		return null;
	}
}
