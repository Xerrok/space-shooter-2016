﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Scripts/Weapons/Weapon System")]
public class WeaponSystem : MonoBehaviour {

	/// <summary>
	/// The vehicle or turret this weapon system is mounted on.
	/// </summary>
	public Vehicle vehicle;
    public WeaponLaser laser;
    public WeaponRocket rocket;
    public MineWeapon mine;


    // Use this for initialization
    void Start () {
		// Make sure the system is properly assigned to the vehicle:
		if (vehicle != null) {
			AssignNewVehicle(vehicle);
		}

        // If no global ballistics simulation system was spawned, instantiate a new one:
        if (GameObject.Find("_BallisticMaster") == null)
        {
            GameObject mgo = new GameObject();
			mgo.AddComponent<BallisticMaster>();
        }

        laser.AssignNewVehicle(vehicle);
		rocket.AssignNewVehicle(vehicle);
//		mine.AssignNewVehicle(vehicle);
    }

	/// <summary>
	/// Assigns this weapon system to a new vehicle or turret.
	/// </summary>
	/// <param name="newVehicle">The vehicle or turret we want to attach this to.</param>
	public void AssignNewVehicle (Vehicle newVehicle) {
		// Make sure the vehicle is non null:
		if (vehicle != null) {
			// Assign vehicle:
			vehicle = newVehicle;
			// Double-check that the weapon system is properly connected to the vehicle:
			if (vehicle.weaponSystem == null) {
				vehicle.AssignNewWeaponSystem(this);
			}
		}
	}

	/// <summary>
	/// Fires the primary weapon installed on this vehicle.
	/// </summary>
	public void FirePrimaryWeapon (float fireAxis = 1.0f) {
        if(laser != null)
        {
			laser.Fire(fireAxis);
        }
	}

	/// <summary>
	/// Fires the secondary weapon installed on this vehicle.
	/// </summary>
	public void FireSecondaryWeapon () {
		// [TODO: Implement weapon pickup and homing missile system first or we can't fire sh*t...]
        if(rocket != null)
        {
            rocket.Fire();
			mine.PlaceMine();
       }
	}
	/*
    public void FireMine()
    {
        if (mine != null)
        {
            mine.PlaceMine();
        }
    }*/
}
