﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class MineProjectile : MonoBehaviour
{
	private bool triggered = false;
    public float Lifetime = 15.0f;
    public float ActivateMine = 2.0f;
    private float MineActivated = 1.0f;


    public Damage damage = new Damage();
    public GameObject explosion;
	private List<Collider> damageList = new List<Collider>();
    

    // Use this for initialization
    void Start()
    {
//        ActivateMine = Time.time;
    }

  
    /// <summary>
    /// Launchs the mine.
    /// </summary>
    /// <param name="source">Source.</param>
    public void LaunchMine(GameObject source = null)
    {
        if (source != null)
        {
            damage.source = source;
        }
    }

    // Update is called once per frame
    void Update()
    {
		if (triggered == true) {
//			Debug.Log("MINE EXPLODIERT! BIATCH!");
			if (damageList != null) {
				foreach (Collider coll in damageList) {
					if (coll != null) {
						coll.gameObject.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
					}
				}
			}
			MineExplosion();
		}

        if (ActivateMine > 0)
        {
            ActivateMine -= Time.deltaTime * MineActivated;
        }
        else
        {
            if (Lifetime > 0)
            {
                Lifetime -= Time.deltaTime * MineActivated;
            }
            else
            {
            	triggered = true;
//                MineExplosion();
            }
        }
    }

    public void MineExplosion()
    {
    	if (explosion != null) {
	    	Instantiate(explosion, transform.position, transform.rotation);
	    }
        Destroy(gameObject);
    }

	public void OnTriggerEnter(Collider _col)
	{
		if (ActivateMine <= 0.0f) {
//			_col.gameObject.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
			triggered = true;
		}
	}

	public void OnTriggerStay (Collider _col) {
		if (triggered == true) {
			damageList.Add(_col);
		}
	}

}