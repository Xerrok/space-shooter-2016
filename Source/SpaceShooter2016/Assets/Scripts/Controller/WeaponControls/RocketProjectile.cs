﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[AddComponentMenu("Scripts/Weapons/Rocket")]
public class RocketProjectile : MonoBehaviour 
{
	public float flightSpeed = 100.0f;
	public float MaxDistance = 100.0f;
	public float lifeTime = 10.0f;
	public float startTime = 0.0f;
	public float rotationSpeed = 5.0f;

	public Damage damage = new Damage();

	private Rigidbody rig = null;
	private Transform target = null;
	public GameObject explosion;

	// Use this for initialization
	void Start () 
	{
		startTime = Time.time;
		rig = GetComponent<Rigidbody>();
	}
	/// <summary>
	/// Launchs the rocket.
	/// </summary>
	/// <param name="source">Source.</param>
	public void LaunchRocket (Transform newTarget = null, GameObject source = null) {
		target = (newTarget != null) ? newTarget : null;
		damage.source = (source != null) ? source : null;
	}

	void OnCollisionEnter(Collision _col){
		_col.gameObject.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
		Detonate();
	}


	// Update is called once per frame

	void FixedUpdate () 
	{
		if (Time.time >= startTime+lifeTime) {
			Detonate();
		}

		if (target != null) {
			Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
		}
		rig.velocity = transform.forward * flightSpeed;
	}

	private void Detonate () {
		if (explosion != null) {
			Instantiate(explosion, transform.position, transform.rotation);
		}
        Destroy(gameObject);
    }

}
