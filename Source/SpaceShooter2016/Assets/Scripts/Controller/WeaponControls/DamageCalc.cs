﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DamageCalc : MonoBehaviour {

	public GameObject explosion;
	public float maxhitpoints;
	public float leftHitpoints = 0.0f;

	private PlayerController playerControl = null;

	public float maxShieldHitpoints;
    public float leftShieldHitpoints = 0.0f;
    public float hitBoxAlive= 0.0f;
    public float currentHitBox = 6.0f;


    void Start()
	{
		leftHitpoints = maxhitpoints;
        maxShieldHitpoints = 0;
		hitBoxAlive = 6.0f;
        leftShieldHitpoints = maxShieldHitpoints;

		if (GetComponent<Vehicle>() && GetComponent<Vehicle>().control != null) {
			playerControl = GetComponent<Vehicle>().control as PlayerController;
 		}
	}

	/// <summary>
	/// Applies the damage.
	/// </summary>
	/// <param name="damage">Damage.</param>
	public void ApplyDamage (Damage damage) {
		
		float restDmg = Mathf.Max(0.0f, damage.damage - leftShieldHitpoints);
        leftShieldHitpoints = Mathf.Max(0.0f, leftShieldHitpoints - damage.damage);
        leftHitpoints -= restDmg;
        
        if (playerControl != null)
        {

            float fillAmountShield = leftShieldHitpoints / maxShieldHitpoints;
            (GetComponent<Vehicle>().control as PlayerController).playerInterface.shieldBarPlayer.fillAmount = fillAmountShield;
            float fillAmount = leftHitpoints / maxhitpoints;
            (GetComponent<Vehicle>().control as PlayerController).playerInterface.healthBar.fillAmount = fillAmount;
        }
        if(leftShieldHitpoints == 0.0f && GetComponent<Vehicle>())
        {
            (GetComponent<Vehicle>().control as PlayerController).playerInterface.shieldIcon.gameObject.SetActive(false);
            (GetComponent<Vehicle>().control as PlayerController).playerInterface.shieldIconCooldown.gameObject.SetActive(false);
        }

		if (leftHitpoints <= 0.0f)
		{
			if (playerControl != null) {
				GameMaster.ReportPlayerKill(new PlayerKill(playerControl, (damage.source != null) ? damage.source : null));
			}
			else if (gameObject.CompareTag("BS_Hitbox")) {
				GameMaster.GetLevelMaster().ApplyBaseDamage(PlayerFaction.EvilRobots);
			}
			else if (gameObject.CompareTag("Hive_Hitbox")) {
				GameMaster.GetLevelMaster().ApplyBaseDamage(PlayerFaction.SpaceBugs);
			}
			CmdDestroyObject(damage);
		}

		if (playerControl != null) {
			float fillAmount = leftHitpoints / maxhitpoints;
			(GetComponent<Vehicle> ().control as PlayerController).playerInterface.healthBar.fillAmount = fillAmount;
		}
	}

	private void CmdDestroyObject (Damage damage) {
		Instantiate(explosion, transform.position, transform.rotation);
		Destroy(gameObject);
	}
}
