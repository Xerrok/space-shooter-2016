﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class is used to store the location and status of a trajectory calculation process.
/// </summary>
public class cBallisticTraceLine {
	/// <summary>
	/// The current position of the projectile in world space.
	/// </summary>
	public Vector3 position = new Vector3();
	/// <summary>
	/// The projectile's velocity in world space.
	/// </summary>
	public Vector3 velocity = Vector3.forward;
	/// <summary>
	/// The magnitude of this projectile's velocity vector.
	/// </summary>
	public float speed = 1.0f;
	/// <summary>
	/// The maximum range of the projectile after which it will be ignored/destroyed/detonated.
	/// </summary>
	public float maxRange = -1.0f;
	/// <summary>
	/// The amount of damage dealt by this projectile impacting a target.
	/// </summary>
	public Damage damage = new Damage(10.0f);
	/// <summary>
	/// The time from when the last trajectory calculation took place.
	/// </summary>
	public float lastCalculationTime = -1.0f;
	/// <summary>
	/// The graphical projectile object to be rendered in scene (shows the projectile's path for awesome effects).
	/// </summary>
	public Transform graphicalProjectile = null;


	// SETTER METHODS:

	/// <summary>
	/// Updates the starting position for the next raycast.
	/// </summary>
	/// <param name="deltaTime">Time difference since last updating projectile status.</param>
	public void UpdatePosition (float deltaTime) {
		// Calculate starting position for next discrete trajectory calculation:
		position += velocity * deltaTime;
		// Shorten maximum remaining range by the distance we covered just now:
		maxRange -= speed * deltaTime;
		// Update position of the graphical projectile:
		if (graphicalProjectile != null) {
			graphicalProjectile.position = position;
		}
	}

	public override string ToString () {
		return "T: v" + speed + ", r" + maxRange + ", d" + damage.damage;
	}

	// CONSTRUCTORS:

	/// <summary>
	/// Initializes a new instance of the <see cref="cBallisticTraceLine"/> class.
	/// </summary>
	/// <param name="p">Initial position of this trajectory calculation.</param>
	/// <param name="v">Initial velocity of this trajectory calculation.</param>
	/// <param name="r">Maximum flight distance of the projectile before interrupting calculation.</param>
	/// <param name="src">The game object of the player/AI who fired this shot.</param>
	/// <param name="dmg">The amount of damage dealt to a target hit by this projectile.</param>
	/// <param name="gp">The graphical projectile to render in scene.</param>
	public cBallisticTraceLine (Vector3 p, Vector3 v, float r, GameObject src, float dmg = 10.0f, Transform gp = null) {
		position = p;
		velocity = v;
		speed = velocity.magnitude;
		maxRange = r;
		lastCalculationTime = -1.0f;
		damage = new Damage(dmg, (src != null) ? src : null);
		graphicalProjectile = (gp != null) ? gp : null;
	}
}

/// <summary>
/// The event of a projectile impacting a target object.
/// </summary>
public class cProjectileImpactEvent {
	/// <summary>
	/// Did the projectile bounce off the target?
	/// </summary>
	public bool ricochet;
	/// <summary>
	/// Did the projectile pierce straight through the target?
	/// </summary>
	public bool pierced;

	/// <summary>
	/// The location in world space where the projectile hit the target GO.
	/// </summary>
	public Vector3 impactPoint;
	/// <summary>
	/// The normal vector of the surface that was impacted.
	/// </summary>
	public Vector3 impactNormal;

	/// <summary>
	/// The actual collider on the target object that was hit.
	/// </summary>
	public Collider impactedCollider;
	/// <summary>
	/// The root transform object of the target we hit.
	/// </summary>
	public Transform impactedObject;

	/// <summary>
	/// Initializes a new instance of the <see cref="cProjectileImpactEvent"/> struct.
	/// </summary>
	/// <param name="p">Impact position in world space.</param>
	/// <param name="n">Impacted surface's normal vector.</param>
	/// <param name="coll">Collider on the target that was hit.</param>
	/// <param name="trans">Root transform object of the target that was hit.</param>
	public cProjectileImpactEvent (Vector3 p, Vector3 n, Collider coll, Transform trans = null) {
		ricochet = false;
		pierced = false;

		impactPoint = p;
		impactNormal = n;

		impactedCollider = coll;
		impactedObject = (trans != null) ? trans : null;
	}
}

/// <summary>
/// The ballistic master script serves as a centralised trajectory plotting system for small and fast projectiles.
/// </summary>
[AddComponentMenu("Scripts/Weapons/Ballistic Master")]
public class BallisticMaster : MonoBehaviour {

	// List conatining all trace requests sent to this master script:
	private cBallisticTraceLine[] tracers = new cBallisticTraceLine[512];

	/// <summary>
	/// The amount of time inbetween calculation of the next step along the trajectory.
	/// </summary>
	public float interval = 0.05f;
	public int counter = 0;

	public Transform graphicalProjectile = null;

	// Use this for initialization
	void Start () {
		// Initialise a new list of projectile tracers:
		if (tracers == null) {
			tracers = new cBallisticTraceLine[512];
		}
		// Force a specific name for this game object:
		// (This way we'll be able to find it at all times!)
		transform.name = "_BallisticMaster";
	}

	/// <summary>
	/// Add a new ballistic projectile trajectory calculation to the list.
	/// </summary>
	/// <param name="p">Initial position of the projectile.</param>
	/// <param name="v">Initial velocity of the projectile.</param>
	/// <param name="r">Maximum simulated flight distance of the projectile.</param>
	/// <param name="src">Source GO from which the projectile was fired.</param>
	/// <param name="gp">Graphical projectile, used solely to visualise the bullet flying by.</param>
	public void CmdAddBallisticProjectile (Vector3 p, Vector3 v, float r, GameObject src, float dmg) {
		if (r > 0.0f && v != Vector3.zero) {
//			Debug.Log("Adding tracer with damage:  d=" + dmg);
			AddTracerToArray(new cBallisticTraceLine(p, v, r, src, dmg));
		}
	}

	/// <summary>
	/// Insert new tracer into simulation array; Internal use only!
	/// </summary>
	/// <returns><c>true</c>, if tracer to array was added, <c>false</c> otherwise.</returns>
	/// <param name="newTracer">New tracer to be added to trajectory simulation.</param>
	private bool AddTracerToArray (cBallisticTraceLine newTracer) {
		// Make sure the tracer is valid and the array has been initialised:
		if (newTracer != null && tracers != null) {
			// Spawn the tracer's graphical projectile:
			if (graphicalProjectile != null) {
				newTracer.graphicalProjectile = Instantiate(graphicalProjectile, newTracer.position,
					Quaternion.LookRotation(newTracer.velocity)) as Transform;
				newTracer.graphicalProjectile.parent = transform;
			}
//			Debug.Log(newTracer.ToString());
			// Go through array and look for free slots:
			for (int i = 0; i<tracers.Length; i++) {
				// If the tracer slot is empty (aka tracer is null):
				if (tracers[i] == null) {
					// Add new tracer here:
					tracers[i] = newTracer;
					return true;
				}
			}
		}
		// Could not be added, return false:
		return false;
	}

	/// <summary>
	/// Use raycasting to determine wether anything was hit by the projectile during this interval.
	/// </summary>
	/// <returns>The projectile impact event, null if no hit was detected.</returns>
	/// <param name="i">The index.</param>
	private cProjectileImpactEvent RaycastTrajectoryPath(int i) {
		// Get the maximum distance covered by the projectile during this single interval:
		float maxDistance = tracers[i].speed * interval;
		RaycastHit hit;
		// Perform a raycast across the flight path of the projectile to detect any collisions:
		if (Physics.Raycast(tracers[i].position, tracers[i].velocity, out hit, maxDistance)) {
			hit.collider.SendMessage("ApplyDamage", tracers[i].damage, SendMessageOptions.DontRequireReceiver);
			cProjectileImpactEvent impact = new cProjectileImpactEvent(hit.point, hit.normal, hit.collider, hit.transform);
//			Debug.Log("T: '" + hit.transform.name + "' was hit.");
			return impact;
		}
		// No collision/impact detected:
		else {
			// Return null:
			return null;
		}
	}

	private void DestroyTracer (int i) {
		// Destroy the graphical projectile:
		if (tracers[i].graphicalProjectile != null) {
			Destroy(tracers[i].graphicalProjectile.gameObject);
		}
		// Detach the tracer: (garbage collector)
		tracers[i] = null;
	}

	// Update is called once per frame
	void FixedUpdate () {
		counter = 0;
		// If the projectile tracer list has already been initialised:
		if (tracers != null) {
			// Go through list of all tracers and update projectile speed and position:
			for (int i = 0; i<tracers.Length; i++) {
				// Find any expired tracers:
				if (tracers[i] != null && tracers[i].maxRange <= 0.0f) {
					// Remove tracer from array:
					DestroyTracer(i);
				}
				// If the tracer is non null:
				if (tracers[i] != null) {
					// Update every so often:
					if (Time.time >= tracers[i].lastCalculationTime + interval) {
						// Update timer of the projectile tracer:
						tracers[i].lastCalculationTime = Time.time;
						// Find out wether the projectile hit something using raycasting:
						cProjectileImpactEvent impact = RaycastTrajectoryPath(i);
						tracers[i].UpdatePosition(interval);

						// Eventually reset the tracer after it hit a target:
						if (impact != null) {
							// Destroy the tracer:
							DestroyTracer(i);
						}
						
						// Have the tracer stand by (aka ignore it) until the timer has run down again.
					}
					counter++;
				}
			}
		}
	}
}
