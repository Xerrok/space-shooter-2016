﻿using UnityEngine;
using System.Collections;


public enum CameraModeTest {
	FirstPersonView,
	OrbitCameraView,
	FollowCameraView,
	None
}

[System.Serializable]
public class CameraSetup {
	public bool relativeSpace = true;
	public bool smoothOrbit = false;
	public bool passiveSmooth = false;

	public CameraModeTest viewPoint = CameraModeTest.OrbitCameraView;

	public Vector3 outsideOffset;
	public Vector3 driverOffset;
	public Transform driverReference;
}

public class MouseOrbitRelative : MonoBehaviour {
	public CameraSetup setup = new CameraSetup();
	public Transform target;
	public float distance = 10.0F;

	public float xSpeed = 250.0F;
	public float ySpeed = 120.0F;
	public float zSpeed = 20.0F;
	
	public float yMinLimit = -20F;
	public float yMaxLimit = 80F;
	public float zMinDist = 5.0F;
	public float zMaxDist = 12.0F;
	
	public float positionSmooth = 0.08F;
	public float rotationSmooth = 0.05F;
	
	private float x = 0.0F;
	private float y = 0.0F;
	private float z = 0.0F;
	
	// Use this for initialization
	void Start () {
		Vector3 angles = transform.eulerAngles;
		x = angles.y;
		y = angles.x;
		// Make the rigid body not change rotation
		if (GetComponent<Rigidbody>()) {
			GetComponent<Rigidbody>().freezeRotation = true;
		}
	}
		
	// LateUpdate is called once after each frame
	void LateUpdate () {
		if (target != null) {
			x += Input.GetAxis("Mouse X") * xSpeed * 0.02F;
			y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02F;
			z = Input.GetAxis("Mouse ScrollWheel");
			if (z > 0.0 && distance > zMinDist) {
				distance -= z * zSpeed * Time.deltaTime;
			}
			else if (z < 0.0 && distance < zMaxDist) {
				distance -= z * zSpeed * Time.deltaTime;
			}

			if (Input.GetKeyDown("v")) {
				setup.viewPoint++;
				if (setup.viewPoint == CameraModeTest.None)
					setup.viewPoint = CameraModeTest.FirstPersonView;
			}
			
			float angle = y;
			if (angle < -360.0F) {
				angle += 360.0F;
			}
			if (angle > 360.0F) {
				angle -= 360.0F;
			}
			y = Mathf.Clamp(angle, yMinLimit, yMaxLimit);

			Vector3 right = target.right;
			Vector3 forward = target.forward;
			Vector3 up = target.up;
			if (setup.relativeSpace == false) {
				right = Vector3.right;
				forward = Vector3.forward;
				up = Vector3.up;
			}

			Vector3 camOffset = setup.outsideOffset;
			if (setup.viewPoint == CameraModeTest.FirstPersonView) {
				camOffset = setup.driverOffset;
			}

			Vector3 lookRight = right * Mathf.Cos(-x * Mathf.Deg2Rad);
			Vector3 lookFor = forward * Mathf.Sin(-x * Mathf.Deg2Rad);
			Vector3 lookUp = up * Mathf.Sin(-y * Mathf.Deg2Rad);
			Vector3 lookDir = lookRight + lookFor + lookUp;
			Vector3 rightDir = Vector3.Cross(up, lookDir);
			Vector3 upDir = Vector3.Cross(rightDir, lookDir);
			Quaternion rotation = Quaternion.LookRotation(lookDir, -upDir);
			Vector3 offset = Vector3.zero;
			offset.z = -distance;

			// Orbit camera:
			if (setup.viewPoint == CameraModeTest.OrbitCameraView) {
				Vector3 position = rotation * offset + target.position + target.TransformDirection(camOffset);
		
				if (setup.smoothOrbit == false) {
					transform.rotation = rotation;
					transform.position = position;
				}
				else {
					Quaternion rot = Quaternion.Slerp(transform.rotation, rotation, rotationSmooth * Time.deltaTime);
					transform.rotation = rot;
					Vector3 smoothSpeed = Vector3.zero;
					Vector3 pos = Vector3.zero;
					if (setup.passiveSmooth == false) {
						pos = Vector3.SmoothDamp(transform.position, position, ref smoothSpeed, positionSmooth);
					}
					else {
						if (Mathf.Abs(x) > 0.0F || Mathf.Abs(y) > 0.0F) {
							pos = position;
						}
						else {
							pos = Vector3.SmoothDamp(transform.position, position, ref smoothSpeed, positionSmooth);
						}
					}
					transform.position = pos;
				}
			}
			// First person camera:
			else if (setup.viewPoint == CameraModeTest.FirstPersonView) {
				if (setup.driverReference == null) {
					setup.driverReference = target.transform;
				}
				transform.rotation = rotation;
				transform.position = setup.driverReference.TransformPoint(setup.driverOffset);
			}
			// Follow camera:
			else {
				// X X X
				// X X X
				// X X X
			}
		}
	}
}