﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using InControl;


/// <summary>
/// Enumerations for camera view
/// </summary>
public enum CameraMode
{
    FirstPersonView,
    FollowCameraView
}

public class CameraController : MonoBehaviour {

    /// <summary>
    /// Camera view mode 
    /// </summary>
    public CameraMode viewMode = CameraMode.FollowCameraView;

    /// <summary>
    /// Pilot (Cockpit) position offset (relative to camera target)
    /// </summary> 
    public Vector3 pilotOffset = new Vector3(0.0f, 0.0f, 2.0f);
    
    /// <summary>
    /// FollowCamera position offset (relative to camera target)
    /// </summary>
    public Vector3 followOffset = new Vector3(0.0f, 3.5f, -10.0f);

    /// <summary>
    /// FollowCamera LookAt point (relative to camera target)
    /// </summary>
    public Vector3 followViewPoint = new Vector3(0.0f, 0.0f, 2.0f);

    /// <summary>
    /// FollowCameraZoom - enables camera distance change with Mouse ScrollWheel on z-axis
    /// </summary> 
    public bool followCameraZoom = true;
    /// <summary>
    /// minimum follow distance on z-axis
    /// </summary>
    public float minFollowDistance = -5.0f;
    /// <summary>
    /// maximum follow distance on z-axis
    /// </summary>
    public float maxFollowDistance = -25.0f;

    /// <summary>
    /// Speed for change the distance between min and max follow distance
    /// </summary>
    [Range(-100.0f, -300.0f)]
    public float zoomSpeed = -200.0f;

    /// <summary>
    /// Cockpit Camera Rotation
    /// </summary>
    public bool cockpitCameraRotation = false;

    /// <summary>
    /// Cockpit camera rotation limit left/right 
    /// </summary>
    public float xLimitCockpit = 80.0f;
    /// <summary>
    /// Cockpit camera rotation limit up/down 
    /// </summary>
    public float yLimitCockpit = 50.0f;

    /// <summary>
    /// smoothCockpitCamera - enables smooth transformation for cockpit camera
    /// </summary>
    public bool smoothCockpitCamera = true;
    
    /// <summary>
    /// smoothTimeCC - approximately the time it will take to reach the target. A heigher value will reach the target faster.
    /// </summary>
    [Range(1.0f, 10.0f)]
    public float smoothTimeCC = 5.0f;

    /// <summary>
    /// smoothFollowCamera - enables smooth transformation for the follow camera
    /// </summary>
    public bool smoothFollowCamera = true;
    /// <summary>
    /// smoothTimeFC - approximately the time it will take to reach the target. A smaller value will reach the target faster.
    /// </summary>
    [Range(0.01f, 1.0f)]
    public float smoothTimeFC = 0.02f;
 

    // CONTROLLER INTERN VARIABLES

    // X input from Mouse/GamePad
    private float x = 0.0F;
    // Y input from Mouse/GamePad
    private float y = 0.0F;
    // Z input from Mouse/GamePad
    private float z = 0.0F;
    // Target rotation for CockpitCamera
    private Quaternion targetRotCC;
    // Target rotation for FollowCamera
    private Quaternion targetRotFC;
    // Target rotation axis for the FollowCamera
    private Vector3 targetAxisFC = Vector3.up;
    // Smooth FollowCamera rotation varaible for calculations
    private float smoothTimeFCRot = 0.0f;
    // Smooth FollowCamera rotation standard value
    private float smoothTimeFCRotStd = 20.0f;
    // Smooth FollowCamera rotation value for ImmelmannTurn
    public float smoothTimeFCRotImmel = 5.0f;
    // velocity value for SmoothDamp function
    private Vector3 velocity = Vector3.zero;
        
    // camTarget allocated over "PlayerControls/Controller" script
    private Transform camTarget;
    // playerSpaceship allocated over "PlayerControls/Controller" script
    private Spaceship playerSpaceship;
	[System.NonSerialized]
	public InputDevice device = null;


	private int playerIndex = 1;


	public void SetPlayerIndex (int newIndex = 1) {
		playerIndex = Mathf.Clamp(newIndex, 1, 4);
	}

    /// <summary>
    /// Set Camera Target
    /// </summary>
    /// <param name="_camTarget"></param>
    public void SetCamTarget(Transform _camTarget)
    {
        this.camTarget = _camTarget;
    }

    /// <summary>
    /// Set Spaceship Component
    /// </summary>
    /// <param name="_spaceship"></param>
    public void SetSpaceship(Spaceship _spaceship)
    {
        this.playerSpaceship = _spaceship;
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        if (camTarget != null)
        {
            // Third Person Mode
            // SmoothDamp funktionierte innerhalb der LateUpdate Methode nicht ohne ruckeln - deswegen in der FixedUpdate Methode    
            if(viewMode == CameraMode.FollowCameraView)
            {                
                followCamera();
            }            
        }
    }

    /// <summary>
    /// LateUpdate is called once after each frame
    /// </summary>
    void LateUpdate()
    {
        if (camTarget != null)
        {
            // Switch CameraMode
			if ((device != null && device.Action4.WasPressed) || Input.GetKeyDown("v"))
            {
                if (viewMode == CameraMode.FollowCameraView)
                {
                    viewMode = CameraMode.FirstPersonView;
                    x = 0.0f;
                    y = 0.0f;
                }
                else
                {
                    viewMode++;
                }
            }

            // First Person Mode
            if (viewMode == CameraMode.FirstPersonView)
            {
                firstPersonCamera();
            }
        }
    }


    /// <summary>
    /// First Person View
    /// </summary>
    private void firstPersonCamera()
    {
        // Rotate Camera if true (GamePad only)
        if (cockpitCameraRotation)
        {
        	if (device != null) {
	            x = device.RightStick.Value.x * xLimitCockpit;
				y = device.RightStick.Value.y * yLimitCockpit;
			}

            x = Mathf.Clamp(x, -xLimitCockpit, xLimitCockpit);
            y = Mathf.Clamp(y, -yLimitCockpit, yLimitCockpit);
            
            targetRotCC = Quaternion.Slerp(targetRotCC, Quaternion.Euler(y, x, 0.0f), smoothTimeCC * Time.deltaTime);
            transform.rotation = camTarget.rotation * targetRotCC;
        } 
        else
        {
            transform.rotation = camTarget.rotation;
        }

        transform.position = camTarget.TransformPoint(pilotOffset);

    }

    /// <summary>
    /// Third Person View
    /// </summary>
    private void followCamera()
    {
        // Change FollowDistance if true
        if (followCameraZoom)
        {
            z = Input.GetAxis("Mouse ScrollWheel");

            if (z > 0.0f && followOffset.z < minFollowDistance)
            {
                followOffset.z -= z * zoomSpeed * Time.deltaTime;
            }
            else if (z < 0.0f && followOffset.z > maxFollowDistance)
            {
                followOffset.z -= z * zoomSpeed * Time.deltaTime;
            }
        }

        // Smooth position changes or not
        if (smoothFollowCamera)
        {
            transform.position = Vector3.SmoothDamp(transform.position, camTarget.TransformPoint(followOffset), ref velocity, smoothTimeFC);
        }
        else
        {
            transform.position = camTarget.TransformPoint(followOffset);
        }


        // Maneuver specific camera rotations
        if ((playerSpaceship.GetManeuver() == EShipManeuver.BarrelRollL ||
            playerSpaceship.GetManeuver() == EShipManeuver.BarrelRollR))
        {
            targetAxisFC = Vector3.up;
            smoothTimeFCRot = smoothTimeFCRotStd;
        }
        else if((playerSpaceship.GetManeuver() == EShipManeuver.ImmelmannTurn ||
                playerSpaceship.GetManeuver() == EShipManeuver.SplitSTurn) && 
                playerSpaceship.GetFlightMode() == EShipFlightMode.Maneuver)
        {
            targetAxisFC = camTarget.up;
            smoothTimeFCRot = smoothTimeFCRotImmel;
        }
        else
        {
            targetAxisFC = camTarget.up;
            smoothTimeFCRot = Mathf.Lerp(smoothTimeFCRot, smoothTimeFCRotStd, Time.deltaTime * 2.0f);
        }

        targetRotFC = Quaternion.LookRotation(camTarget.TransformPoint(followViewPoint) - transform.position, targetAxisFC);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotFC, Time.deltaTime * smoothTimeFCRot);        
    }


    /*
    // ***************
    // * Debug Stuff *
    // ***************
    void OnGUI()
    {
        if (debugOutput)
        {
            debugText  = "Spaceship localEulerAngles -> " + camTarget.localEulerAngles.ToString() + "\n";
            debugText += "Camera localEulerAngles -> " + transform.localEulerAngles.ToString() + "\n\n";
            debugText += "Camera Position -> " + transform.position.ToString();

            GUI.Label(debugLabeRect, "Debug Output CameraController:\n" + debugText);
        }
    }
    */

    void OnDrawGizmos()
    {
		if (camTarget != null)
        {
            Gizmos.color = Color.blue;
            Vector3 direction = camTarget.TransformDirection(Vector3.forward) * 50;
            Vector3 pos1 = new Vector3(0.0f, -1.0f, 2.0f);
            Gizmos.DrawRay(camTarget.TransformPoint(pos1), direction);

            Vector3 pos2 = new Vector3(-5.0f, 0.0f, 15.0f);
            Gizmos.DrawRay(transform.TransformPoint(pos2), Vector3.forward);
            Gizmos.color = Color.green;
            Gizmos.DrawRay(transform.TransformPoint(pos2), Vector3.up);
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.TransformPoint(pos2), Vector3.right);            
        }

    }

}
