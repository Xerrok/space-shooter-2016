﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[AddComponentMenu("Scripts/Control/Player Controller")]
public class SpaceshipAI : Controller
{
    // Distanz, die er zum Target einnimmt, vermeidet Collission
    public float magnitudeDistance;
    public float maxSpeed;
    public Vector3 target;
    public float rotationSpeed;
    public float attackRange = 200;
    public float normalRaycastLength;
    public float attackRaycastLength;
    public float secondaryFireRate;
    public float dogFightSearchTime = 1;
    public string enemyTagName;
    public float enemySearchDistance;
    public int primaryFireSalve;
    public float primaryFireRate;


    public float evadeDistance;
    public float wingSpan;

    // Speed Variables, maxSpeed is used for the actual velocity, 
    // normalSpeed and attackSpeed defines the new targetSpeed in each state (without boost)
    // targetSpeed = normalSpeed; || targetSpeed = attackSpeed;
    public float normalSpeed;
    public float attackSpeed;
    private float targetSpeed;

    // Factor in which the spaceship accelerates from the current maxSpeed to the targetSpeed
    public float accelerationFactor;

    private int closest_i;
    private Vector3 storeTarget;
    private Vector3 newTargetPos;
    private Transform obstaclePos;
    private Vector3 accelerationDir;
    private Vector3 velocity;
    private Rigidbody rigidBody;
    private bool savePos;
    private bool overrideTarget;
    private float raycastLength;
    private LayerMask ignoreLayer;
    private float secondaryFireTimer;
    private DamageCalc healthPoints;
    private bool b_dogFight;
    private float dogFightTimer;
    private DamageCalc enemyHealth;
    private GameObject dogFight;
    private float primaryFireTimer;
    private bool activatePrimaryFire;
    private int primaryFireSalveCount;
    private bool isEvading;
    private float tempDistance;


    private float timer;

    public WeaponSystem weaponSystem = null;

    protected override void Initialise()
    {
    }
    public override void FetchInput()
    {
    }
    public enum AIStates
    {
        AI_normal,
        AI_attack
    }

    public AIStates aiState;

    public List<Vector3> EscapeDirections = new List<Vector3>();

    // Use this for initialization
    void Start ()
    {
        rigidBody = GetComponent<Rigidbody>();
        savePos = false;

        target = GameObject.FindGameObjectWithTag("Target").transform.position;
        storeTarget = target;

        secondaryFireTimer = secondaryFireRate;
        activatePrimaryFire = true;
        b_dogFight = false;

    }
	
	public override void SetSpawnPoint (Transform _transform) {
		return;
	}
	public override void InitSpawnPoint (PlayerFaction faction) {
		return;
	}
	protected override void StartSpawnTimer () {
		return;
	}
	protected override void UpdateRespawnTimer () {
		return;
	}
	protected override void UpdateRespawnPunishmentTimer () {
		return;
	}


	// Update is called once per frame
	void FixedUpdate ()
    {
        // Assign new Target

        //Debug.DrawLine(transform.position, target.position);
        ManageSpeed();

        /**************************************************************************************************************/
        /////////////////
        // Statechange //
        /////////////////

        switch (aiState)
        {
            case AIStates.AI_normal:
                raycastLength = normalRaycastLength;
                targetSpeed = normalSpeed;
                break;

            case AIStates.AI_attack:
                raycastLength = attackRaycastLength;
                targetSpeed = attackSpeed;
                break;
        }

        /**************************************************************************************************************/

        /**************************************************************************************************************/
        //////////////////
        // Search Enemy //
        //////////////////

        if (b_dogFight == false)
        {
            dogFightTimer += Time.deltaTime;

            // Enemy Searcher, if one enemy is in his radius, he'll change his target

            if (dogFightTimer > dogFightSearchTime)
            {
                target = storeTarget;

                if (isEvading == false)
                {
                    if (dogFight == null)
                    {
                        dogFight = dogFightSearch();
//                        Debug.Log("fightsearch ausgeführt");

                        if (dogFight != null)
                        {

                            if (Physics.Linecast(transform.position, dogFight.transform.position))
                            {
                                if (dogFight.transform.gameObject.CompareTag("Enemy"))
                                {
                                    target = dogFight.transform.position;
                                    enemyHealth = dogFight.transform.GetComponent<DamageCalc>();
                                    b_dogFight = true;
                                }
                            }
                        }
                    }
                }

                dogFightTimer = 0;
            }
        }

        else
        {
            // Switch to old target, when enemy is dead
            if (enemyHealth == null)
            {
                target = storeTarget;
                b_dogFight = false;
                dogFight = null;
                Debug.Log("enemydestroy");
            }

            // Follow enemy
            else
            {
                if (dogFight != null)
                {
                    target = dogFight.transform.position;
                }
            }
        }

        /**************************************************************************************************************/


        Debug.DrawRay(transform.position + new Vector3(0.5f, 0, 0), transform.forward * 200, Color.blue);
        Debug.DrawRay(transform.position - new Vector3(0.5f, 0, 0), transform.forward * 200, Color.red);

        // Attacking


        RaycastHit attackRay;

        if (Physics.Raycast(transform.position, transform.forward, out attackRay, attackRange))
        {
            if (attackRay.transform.GetComponent<DamageCalc>() != null)
            {

                secondaryFireTimer += Time.deltaTime;
                primaryFireTimer += Time.deltaTime;

                if (secondaryFireRate < secondaryFireTimer)
                {
                    if (attackRay.transform.CompareTag("Target") || attackRay.transform.CompareTag(enemyTagName))
                    {
                        weaponSystem.FireSecondaryWeapon();
                        secondaryFireTimer = 0;

                        Debug.Log("Missile pew.");
                    }
                }

                // Fire the primary weapon in a salve
                if (primaryFireRate < primaryFireTimer && activatePrimaryFire)
                {
                    weaponSystem.FirePrimaryWeapon();
                    primaryFireSalveCount++;
                    primaryFireTimer = 0;

                    if (primaryFireSalveCount == primaryFireSalve)
                    {
                        activatePrimaryFire = false;
                    }
                }

                // Pause between the salves
                if (primaryFireTimer > 1 && activatePrimaryFire == false && primaryFireSalveCount == primaryFireSalve)
                {
                    activatePrimaryFire = true;
                    primaryFireTimer = 0;
                    primaryFireSalveCount = 0;
                }

            }
        }

        /**************************************************************************************************************/
        //////////////
        // Movement //
        //////////////

        Vector3 forces = MoveTowardsTarget(target);

        accelerationDir = forces;

        if (accelerationDir != Vector3.zero)
        {
            velocity += rotationSpeed * accelerationDir * Time.deltaTime;


            if (velocity.magnitude > maxSpeed)
            {
                velocity = velocity.normalized * maxSpeed;
            }
        }

            rigidBody.velocity = velocity;
        

        Quaternion desiredRotation = Quaternion.LookRotation(velocity);
        transform.rotation = Quaternion.Slerp(transform.rotation, desiredRotation, Time.deltaTime * 5);

        /**************************************************************************************************************/

        timer += Time.deltaTime;



        // Looks for Obstacles
        if (timer < 0.5f)
        {
            //    if (!ignoreEnemy)
            //    {
            ObstacleAvoidance(velocity, 0);

            if (overrideTarget)
            {
                target = newTargetPos;
                isEvading = true;
            }

            timer = 0;
            //  }

        }
    }

    Vector3 MoveTowardsTarget (Vector3 _target)
    {
        Vector3 distance = _target - transform.position;

        if (distance.magnitude < magnitudeDistance)
        {
            return distance.normalized * -maxSpeed;
        }

        else
        {
            return distance.normalized * maxSpeed;
        }
    }

    void ObstacleAvoidance(Vector3 direction, float offsetX)
    {
       RaycastHit hit =  Rays(direction, offsetX);

        if (hit.transform != null && hit.transform.gameObject != this.gameObject)
        {
            if(!savePos && dogFight == null)
            {
                storeTarget = target;
                savePos = true;
            }
          

            FindEscapeDirections(hit.collider);
        }

        if (EscapeDirections.Count > 0)
        {
            if (!overrideTarget)
            {
                newTargetPos = getClosests();
                overrideTarget = true;
            }
        }


            tempDistance = Vector3.Distance(transform.position, target);

            if (tempDistance < wingSpan)
            {
                if (savePos)
                {
                    target = storeTarget;
                    savePos = false;
                }

                isEvading = false;

                overrideTarget = false;

                EscapeDirections.Clear();
            }

    }

    Vector3 getClosests()
    {
        Vector3 clos = EscapeDirections[0];
        float distance = Vector3.Distance(transform.position, EscapeDirections[0]);

        // Calculates the shortest distance
        for (int i = 0; i < EscapeDirections.Count; i++)
        {
            float tempDistance = Vector3.Distance(transform.position, EscapeDirections[i]);
            //Debug.DrawRay(transform.position , -(transform.position + EscapeDirections[i]) * tempDistance, Color.red, 100);

            if (!Physics.Linecast(transform.position, EscapeDirections[i]))
            {
                if (tempDistance < distance)
                {
                    distance = tempDistance;
                    clos = EscapeDirections[i];
                }
            }
        }


        return clos;
    }

    void FindEscapeDirections(Collider col)
    {
        // Hitting up
        RaycastHit hitUp;

        if (Physics.Raycast(col.transform.position, Vector3.up, out hitUp, col.bounds.extents.y + evadeDistance + wingSpan))
        { }

        else
        {
            Vector3 dir = col.transform.position + new Vector3(0, (col.bounds.extents.y + evadeDistance + wingSpan), 0);

            if (!EscapeDirections.Contains(dir))
            {
                EscapeDirections.Add(dir);
            }
        }


        // Hitting down
        RaycastHit hitDown;

        if (Physics.Raycast(col.transform.position, Vector3.down, out hitDown, col.bounds.extents.y + evadeDistance + wingSpan))
        { }

        else
        {
            Vector3 dir = col.transform.position + new Vector3(0, -(col.bounds.extents.y + evadeDistance + wingSpan), 0);

            if (!EscapeDirections.Contains(dir))
            {
                EscapeDirections.Add(dir);
            }
        }

        // Hitting right
        RaycastHit hitRight;

        if (Physics.Raycast(col.transform.position, Vector3.right, out hitRight, col.bounds.extents.x + evadeDistance + wingSpan))
        { }

        else
        {
            Vector3 dir = col.transform.position + new Vector3((col.bounds.extents.x + evadeDistance + wingSpan), 0, 0);

            if (!EscapeDirections.Contains(dir))
            {
                EscapeDirections.Add(dir);
            }
        }

        // Hitting left
        RaycastHit hitLeft;

        if (Physics.Raycast(col.transform.position, Vector3.left, out hitLeft, col.bounds.extents.x + evadeDistance + wingSpan))
        { }

        else
        {
            Vector3 dir = col.transform.position + new Vector3(-(col.bounds.extents.x + evadeDistance + wingSpan), 0, 0);

            if (!EscapeDirections.Contains(dir))
            {
                EscapeDirections.Add(dir);
            }
        }

        //// Hitting forward
        //RaycastHit hitForward;

        //if (Physics.Raycast(col.transform.position, Vector3.forward, out hitForward, col.bounds.extents.x + evadeDistance + wingSpan))
        //{ }

        //else
        //{
        //    Vector3 dir = col.transform.position + new Vector3((col.bounds.extents.x + evadeDistance + wingSpan), 0, 0);

        //    if (!EscapeDirections.Contains(dir))
        //    {
        //        EscapeDirections.Add(dir);
        //    }
        //}

        //// Hitting backward
        //RaycastHit hitBackward;

        //if (Physics.Raycast(col.transform.position, Vector3.back, out hitBackward, col.bounds.extents.x + evadeDistance + wingSpan))
        //{ }

        //else
        //{
        //    Vector3 dir = col.transform.position + new Vector3(-(col.bounds.extents.x + evadeDistance + wingSpan), 0, 0);

        //    if (!EscapeDirections.Contains(dir))
        //    {
        //        EscapeDirections.Add(dir);
        //    }
        //}
    }


    RaycastHit Rays(Vector3 direction, float offsetX)
    {
        Ray ray = new Ray(transform.position + new Vector3(offsetX, 0, 0), direction);
        RaycastHit rayHit;
        Physics.Raycast(ray, out rayHit, maxSpeed * raycastLength);

        return rayHit;
    }

    void ManageSpeed()
    {
        maxSpeed = Mathf.MoveTowards(maxSpeed, targetSpeed, Time.deltaTime * accelerationFactor);
    }

    GameObject dogFightSearch()
    {
       GameObject dogfightEnemy = null;

        if (b_dogFight == false)
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTagName);
            float closestDistance = Mathf.Infinity;

            foreach (GameObject enemy in enemies)
            {               
                float enemyDistance = Vector3.Distance(enemy.transform.position, transform.position);

                if (enemyDistance <= enemySearchDistance)
                {
                    if (Vector3.Angle(transform.position, enemy.transform.position) < 5)
                    {
                        if (enemyDistance < closestDistance)
                        {
                            dogfightEnemy = enemy;
                            closestDistance = enemyDistance;

                        }
                    }
                }

            }
        }


        return dogfightEnemy;

    }
 }
