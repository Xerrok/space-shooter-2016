﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using InControl;

/// <summary>
/// A vehicle/ship controller specific for use by human players.
/// </summary>
[AddComponentMenu("Scripts/Control/Player Controller")]
public class PlayerController : Controller {

	public AnimationCurve axisAcceleration = new AnimationCurve(new Keyframe(0.0f, 0.5f), new Keyframe(1.0f, 1.0f));

	private int playerIndex = 1;
	public CameraController cameraCtrl = null;
	[System.NonSerialized]
	public PlayerMenu playerMenu = null;

	public PlayerInterface playerInterface = null;
	[System.NonSerialized]
	public InputDevice device = null;

	/// <summary>
    /// Time to wait for respawn
    /// </summary>
	public float respawnTime = 10.0f;
    /// <summary>
    /// Time value to increase respawn time  
    /// </summary>
    public float respawnIncreaseTime = 10.0f;
    /// <summary>
    /// Punishment period after player respawn
    /// </summary>
    public float respawnPunishmentPeriod = 30.0f;
    /// <summary>
    /// Current time for respawnTimer and respawnPunishmentTimer  
    /// </summary>
    private float respawnCurrentTime = 0.0f;
    /// <summary>
    /// Total respawn time (respawn time + increase time)
    /// </summary>
    private float respawnTotalTime = 0.0f;

	public int GetPlayerIndex () {
		return playerIndex;
	}
	public void SetPlayerIndex (int newIndex = 1) {
		playerIndex = Mathf.Clamp(newIndex, 1, 4);
		cameraCtrl.SetPlayerIndex(playerIndex);
		playerMenu = transform.parent.GetComponent<PlayerMenu>();
		playerMenu.SetPlayerIndex(playerIndex);
	}


	public void AssignNewInputDevice (InputDevice newDevice) {
		device = newDevice;
		cameraCtrl.device = newDevice;
	}

	protected override void Initialise () {
		if (playerIndex != 1) {
			GetComponent<AudioListener>().enabled = false;
		}
		// initialise respawn time
        respawnTotalTime = respawnTime;
	}

	/// <summary>
    /// Set a spawn point for this controller:
    /// </summary>
    /// <param name="_spawnPoint">The new spawnpoint.</param>
    public override void SetSpawnPoint(Transform _transform)
    {
        if (_transform != null)
        {
            transform.position = _transform.position;
            transform.rotation = _transform.rotation;
//            Debug.Log("Player " + playerIndex + " startet von " + _transform.name);
        }
        else
        {
            Debug.LogError("Player " + playerIndex + " Spawn Punkt Fehler!");
        }
    }

    /// <summary>
    /// Initialise spawn point:
    /// </summary>
    /// <param name="_spawnPoint">bla...</param>
    public override void InitSpawnPoint(PlayerFaction newFaction = PlayerFaction.Spectator)
    {
        // Achtung!! Zugriff muss über Parent Object (Battleship/Hive) erfolgen!!! 
        if (newFaction == PlayerFaction.EvilRobots)
        {
            GameObject.Find("Mothership").GetComponentInChildren<DockController>().RequestLaunchingPlace(gameObject);
        }
        else if (newFaction == PlayerFaction.SpaceBugs)
        {
			GameObject.Find("Hive").GetComponentInChildren<DockController>().RequestLaunchingPlace(gameObject);
        }
		playerInterface.ChangeFactionUI(newFaction);
    }

	/// <summary>
    /// Start timer for respawn 
    /// </summary>
    protected override void StartSpawnTimer()
    {
        // is respawn punishment timer active, increase the respawn timer
        if (respawnPunishmentTimerActive)
        {
            respawnTotalTime += respawnIncreaseTime;
//            Debug.Log("Punishment timer was active! -> Respawn time increased!");
        }
        else
        {
            respawnTotalTime = respawnTime;
        }


        respawnCurrentTime = respawnTotalTime;
        respawnTimerActive = true;
//        Debug.Log("Respawn timer activated!");

        playerInterface.respawnTimer.text = respawnCurrentTime.ToString();
        playerInterface.respawnTimer.gameObject.SetActive(true);
    }

    /// <summary>
    /// Update respawn timer - if respawn time expired, spawn new vehicle 
    /// </summary>
    protected override void UpdateRespawnTimer()
    {
        playerInterface.respawnTimer.text = Mathf.Round(respawnCurrentTime).ToString();
        
        respawnCurrentTime -= Time.deltaTime;

        if (respawnCurrentTime < 0.0f)
        {
            Vehicle vehicleNeu = null;

            if (faction == PlayerFaction.EvilRobots)
            {
                vehicleNeu = GetComponentInParent<PlayerMenu>().robotShip;
            }
            else if (faction == PlayerFaction.SpaceBugs)
            {
                vehicleNeu = GetComponentInParent<PlayerMenu>().insectShip;
            }

            if (vehicleNeu != null)
            {
                InitSpawnPoint(faction);
                SpawnVehicle(vehicleNeu);
                Debug.Log("New ship spawned!");
            }

            // deactivate respawn timer
            respawnTimerActive = false;
//            Debug.Log("Respawn timer deactivated!");
            // activate punishment timer
            respawnPunishmentTimerActive = true;
//            Debug.Log("Punishment timer activated!");
            // hide canvas object
            playerInterface.respawnTimer.gameObject.SetActive(false);
            // set punishment time for respawn punishment update method
            respawnCurrentTime = respawnPunishmentPeriod;
        }
    }

    /// <summary>
    /// Update respawn punishment timer 
    /// </summary>
    protected override void UpdateRespawnPunishmentTimer()
    {
        respawnCurrentTime -= Time.deltaTime;

        if (respawnCurrentTime < 0.0f)
        {
            respawnPunishmentTimerActive = false;
 //           Debug.Log("Punishment timer deactivated! Respawn time will be normalized!");
        }

    }


	/// <summary>
	/// Fetch direct input commands from a human player.
	/// </summary>
	public override void FetchInput () {
		// ROTATION INPUT:

		float rawAxisX = Input.GetAxis("Vertical");
		float rawAxisY = Input.GetAxis("Horizontal");
		if (device != null) {
			rawAxisX = device.Direction.Y;
			rawAxisY = device.Direction.X;
		}
		else {
			if (playerIndex > 1) {
				Debug.Log("ERROR! Controller not set!");
			}
		}
		// Go up or down, aka pitch control:
		inputAxis.x = rawAxisX * axisAcceleration.Evaluate(Mathf.Abs(rawAxisX));
		// Go left or right, aka yaw control:
		inputAxis.y = rawAxisY * axisAcceleration.Evaluate(Mathf.Abs(rawAxisY));

		// SPACESHIP SPEED:

		// Use boost to go faster:
		if (Input.GetKey(KeyCode.Space)) {
			acceleration = 1.0f;
		}
		// None of those buttons were pressed, return to normal speed:
		else {
			acceleration = 0.0f;
		}
		// Also use gamepad's left trigger for boost:
		// (Value is clamped to prevent user from abusing keyborad while playing on gamepad!)
		if (device != null) {
			acceleration = Mathf.Clamp01(acceleration + device.LeftTrigger.Value);
			deceleration = (boost == false && device.Action2.IsPressed) ? -1.0f : 0.0f;

			boost = device.LeftBumper.IsPressed;
		}
		

		if (vehicle != null) {
			// MANOEUVERS:

			GetManeuverInput();

			// WEAPONRY:

			GetWeaponInput();
		}

		// Tell the player menu to update the radar/minimap on the player's ingame UI:
		if (playerMenu != null) {
			playerMenu.UpdatePlayerRadar();
		}

/*		if (vehicle == null)
        {
     		InitSpawnPoint(faction);
    		SpawnVehicle((faction == PlayerFaction.EvilRobots) ? robotShip : insectShip);
        }
*/
	}

	/// <summary>
	/// Gets the input responsible for triggering various maneuvers.
	/// </summary>
	private void GetManeuverInput () {
		// Rolling maneuvers:
		if ((device != null && device.Action1.WasPressed) || Input.GetKeyDown(KeyCode.LeftShift)) {
			// Barrel roll left:
			if (inputAxis.y < -0.1f && Mathf.Abs(inputAxis.x) < 0.5f) {
				vehicle.StartManeuver(EShipManeuver.BarrelRollL);
			}
			// Barrel roll right:
			else if (inputAxis.y > 0.1f && Mathf.Abs(inputAxis.x) < 0.5f) {
				vehicle.StartManeuver(EShipManeuver.BarrelRollR);
			}
			// Immelmann turn: (up)
			else if (inputAxis.x <= -0.5f) {
				vehicle.StartManeuver(EShipManeuver.ImmelmannTurn);
			}
			// Split-S turn: (down)
			else if (inputAxis.x >= 0.5f) {
				vehicle.StartManeuver(EShipManeuver.SplitSTurn);
			}
		}
	}

	/// <summary>
	/// Gets the input responsible for firing any weapons mounted on the vehicle.
	/// </summary>
	private void GetWeaponInput () {
		// MOUSE BUTTON INPUT:
		if (Input.GetMouseButton(0)) {
			vehicle.weaponSystem.FirePrimaryWeapon(1.0f);
		}
		if (Input.GetMouseButtonDown(1)) {
			vehicle.weaponSystem.FireSecondaryWeapon();
		}
		if (device != null) {
			// GAMEPAD TRIGGER INPUT:
			float fireAxis = device.RightTrigger.Value;
			if (fireAxis > 0.05f) {
				vehicle.weaponSystem.FirePrimaryWeapon(fireAxis);
			}
			if (device.RightBumper.WasPressed) {
				vehicle.weaponSystem.FireSecondaryWeapon();
			}
		}
	}

}