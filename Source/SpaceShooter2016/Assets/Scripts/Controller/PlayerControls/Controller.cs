﻿using UnityEngine;
using System.Collections;

public enum PlayerFaction {
	EvilRobots,
	SpaceBugs,
	Spectator
}

/// <summary>
/// This controls and returns any input values and commands issued by either a human or AI player.
/// </summary>
public abstract class Controller : MonoBehaviour {

	public KillDeathRatio killDeathRatio = new KillDeathRatio();

	public bool boost = false;
	/// <summary>
	/// The acceleration/boost of the vehicle, aka wether to use normal or higher speed.
	/// </summary>
	public float acceleration = 0.0f;
	/// <summary>
	/// The deceleration/break of the vehicle, aka wether to reeeeaaaal slow.
	/// </summary>
	public float deceleration = 0.0f;
	/// <summary>
	/// Rotation axis input, mainly only pitch (x-axis) and yaw (y-axis).
	/// </summary>
	public Vector3 inputAxis = new Vector3();

    // PLAYER SECIFIC STUFF:

	/// <summary>
	/// The vehicle/spaceship/turret or whatever the hell this behaviour is controlling right now.
	/// </summary>
	public Vehicle vehicle = null;

	/// <summary>
	/// The faction this player belongs to.
	/// </summary>
	[System.NonSerialized]
	public PlayerFaction faction = PlayerFaction.EvilRobots;

	/// <summary>
	/// Wether this player is carrying a mine.
	/// </summary>
	[System.NonSerialized]
	public bool isCarryingMine = false;

	/// <summary>
    /// Is timer for respawn active?
    /// </summary>
    protected bool respawnTimerActive = false;
    /// <summary>
    /// Is punishment timer after respawn active?
    /// </summary>
    protected bool respawnPunishmentTimerActive = false;


	// Use this for initialization
	void Start () {
		if (vehicle != null) {
			vehicle.AssignNewController(this);
		}
		Initialise();
	}

	/// <summary>
	/// Initialise all states on this instance.
	/// </summary>
	protected abstract void Initialise ();

	public void SetFaction (PlayerFaction newFaction = PlayerFaction.Spectator) {
		faction = newFaction;
		// [TODO: Asign player tags or other stuff like that...]
	}

	/// <summary>
	/// Spawns a new vehicle for this controller, call once on Start and for Respawns.
	/// </summary>
	public void SpawnVehicle (Vehicle newVehicle) {
//		Debug.Log("Ship spawned!");
		GameObject v = Instantiate(newVehicle.gameObject, transform.position, transform.rotation) as GameObject;
		vehicle = v.GetComponent<Vehicle>();
		vehicle.AssignNewController(this);
		GetComponent<CameraController>().SetCamTarget(v.transform);
        GetComponent<CameraController>().SetSpaceship(v.GetComponent<Spaceship>());
        vehicle.StartDocking();
	}

	/// <summary>
	/// Assign a new vehicle to be controlled by this behaviour:
	/// </summary>
	/// <returns><c>true</c>, if a new vehicle was assigned, <c>false</c> if already assigned or invalid ship.</returns>
	/// <param name="newShip">The new ship we want to assign this controller to.</param>.</param>
	public bool AssignNewVehicle (Vehicle newShip) {
		if (newShip != null && vehicle == null) {
			vehicle = newShip;
			newShip.AssignNewController(this);
			return true;
		}
		return false;
	}

	public void PlayersKilled (int newKills = 1) {
//		Debug.Log("You killed a player.");
		killDeathRatio.kills += newKills;
	}
	public void PlayerDied () {
//		Debug.Log("You were killed.");
		killDeathRatio.deaths++;
//		Debug.Log("Activate respawn timer");
        StartSpawnTimer();
	}

	/// <summary>
	/// Fetch input commands from either player or AI controller subclasses.
	/// </summary>
	public abstract void FetchInput ();
	
    // Update is called once per frame
	void Update () {
		FetchInput();

        // Respawn timer active? ... manage update methods! 
        if (respawnTimerActive)
        {
            UpdateRespawnTimer();
        }
        else if (respawnPunishmentTimerActive)
        {
            UpdateRespawnPunishmentTimer();
        }

	}


	/// <summary>
    /// Set a spawn point for this controller:
    /// </summary>
    /// <param name="_spawnPoint">The new spawnpoint.</param>
    public abstract void SetSpawnPoint(Transform _transform);

    /// <summary>
    /// Initialise spawn point:
    /// </summary>
    /// <param name="_spawnPoint">bla...</param>
    public abstract void InitSpawnPoint(PlayerFaction newFaction = PlayerFaction.Spectator);

    /// <summary>
    /// Start timer for respawn 
    /// </summary>
    protected abstract void StartSpawnTimer();

    /// <summary>
    /// Update respawn timer - if respawn time expired, spawn new vehicle 
    /// </summary>
    protected abstract void UpdateRespawnTimer();

    /// <summary>
    /// Update respawn punishment timer 
    /// </summary>
    protected abstract void UpdateRespawnPunishmentTimer();
}
