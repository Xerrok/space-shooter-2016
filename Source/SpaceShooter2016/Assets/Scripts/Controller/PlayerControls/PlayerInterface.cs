﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

[System.Serializable]
public class PlayerRadarInterface {
	public Image player = null;
	public Image[] allies = new Image[3];
	public Image[] enemies = new Image[4];
	public Image techBase = null;
	public Image hiveBase = null;
	public Image[] pickups = new Image[6];

	public float radarRange = 600.0f;
	public float depthMinScale = 0.25f;
	public float depthMaxScale = 2.0f;
}

[System.Serializable]
public class PlayerScoreBoard {
	public GameObject canvas = null;
	public Text robotTeamScore = null;
	public Text insectTeamScore = null;
	public Text[] robotNames = new Text[4];
	public Text[] robotScores = new Text[4];
	public Text[] insectNames = new Text[4];
	public Text[] insectScores = new Text[4];

	// [TODO/INCOMPLETE]
	public void UpdateScoreBoard () {
		Controller[] players = GameMaster.players;
		int bugCounter = 0;
		int botCounter = 0;
		KillDeathRatio botKD = new KillDeathRatio();
		KillDeathRatio bugKD = new KillDeathRatio();

		for (int i = 0; i<players.Length; i++) {
			if (players[i] != null) {
				string kdTxt = players[i].killDeathRatio.ToString();
				if (players[i].faction == PlayerFaction.EvilRobots) {
					botKD.kills += players[i].killDeathRatio.kills;
					botKD.deaths += players[i].killDeathRatio.deaths;
					robotNames[botCounter].text = players[i].transform.name;
					robotScores[botCounter].text = kdTxt;
					botCounter++;
				}
				else if (players[i].faction == PlayerFaction.SpaceBugs) {
					bugKD.kills += players[i].killDeathRatio.kills;
					bugKD.deaths += players[i].killDeathRatio.deaths;
					insectNames[bugCounter].text = players[i].transform.name;
					insectScores[bugCounter].text = kdTxt;
					bugCounter++;
				}
			}
		}
		robotTeamScore.text = botKD.ToString();
		insectTeamScore.text = bugKD.ToString();
	}
}

[System.Serializable]
public class PlayerBriefingTab {
	public GameObject parentObj = null;
	public Image portrait = null;
	public Text commanderTitle = null;
	public Text briefingText = null;
}

/// <summary>
/// Das hier ist eigentlich nur eine Referenz für andere Scripts packt bitte nichts rein, was hier nicht hingehört.
/// </summary>
public class PlayerInterface : MonoBehaviour {

	// PACKT HIER BITTE AUCH KEINE UPDATE METHODEN REIN!

	// Das Projekt ist eh schon verschachtelt genug...

	public Image boostIcon = null;
	public Image mineIcon = null;
	public Image rocketIcon = null;
	public Image shieldIcon = null;
    public Image invisibleIcon = null;

	public Image maneuverIcon = null;
	public Image maneuverIconCooldown = null;

	public Image crosshair = null;

	public Image healthBar = null;
	public Image healthBarEnemy = null;
	public Image shieldBarPlayer = null;

	public Image boostBarFull = null;
	public Image boostBarEmpty = null;

	public Image overheatBar = null;

    public Image boostIconCooldown = null;
    public Image shieldIconCooldown = null;
    public Image invisibilityIconCooldown = null;

	public PlayerRadarInterface radarInterface = new PlayerRadarInterface();
	public PlayerScoreBoard scoreBoard = new PlayerScoreBoard();

	public Text winText = null;
    public Text ammoMines = null;
    public Text ammoMissiles = null;

	public Image bsHealthBar = null;
	public Image hiveHealthBar = null;

	public Sprite topPanelBugs =null;
	public Image topPanelTech =null;

	public Image skillBarTech =null;
	public Sprite skillBarBugs = null;

	public Text respawnTimer = null;

	public Text launchModeActive = null;

	public PlayerBriefingTab briefing = new PlayerBriefingTab();


	// FACTION SELECTION:

	[System.NonSerialized]
	public int joinButtonSelected = 0;
	[System.NonSerialized]
	public float joinButtonScroll = 0.0f;
	public Image[] joinFactions = new Image[3];


	public void ChangeFactionUI(PlayerFaction faction)
	{
		if (faction == PlayerFaction.SpaceBugs)
		{
			topPanelTech.sprite = topPanelBugs;
			skillBarTech.sprite = skillBarBugs;
//			Debug.Log("Panels swaped");
		}
	}
}


