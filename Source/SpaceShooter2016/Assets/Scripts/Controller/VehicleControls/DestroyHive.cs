﻿using UnityEngine;
using System.Collections;

public class DestroyHive : MonoBehaviour
{
    public GameObject explosion;
    public GameObject hive;
    private bool hiveDestroyed = false;
    public float time =10.0f;

   
    void Update()
    {
        TimeUntilHiveIsDestroyed();
        // bla
    }

    
    public void TimeUntilHiveIsDestroyed()
    {
       
        time -= Time.deltaTime;
        if (time <=0.0f && hiveDestroyed ==false)
        {
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(hive.gameObject);
            hiveDestroyed = true; 
        }
    }
}
