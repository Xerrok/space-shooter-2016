﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class BattleshipFlightControl : MonoBehaviour {

    public GameObject explosion;
    public float battleshipSpeed = 5.0f;
    public float distanceToAsteroidForStop = 50.0f;
    public float accelerationFade = 2.0f;
    public float timerValue = 20.0f;


    private Rigidbody rig;
    public Transform barrier;
    public Transform hive;
    public Transform frontPoint;
    public Transform targetPoint;
    private int asteroidCounter = 0;
    private float targetZ = 0.0f;
    private float frontZ = 0.0f;
    private bool timerActive = false;
    private float time = 0.0f;

    private bool destroyed = false;
    public GameObject mainCannonFire = null;
	public float hiveExplosionDelay = 2.0f;

    // ***************
    // * Debug Stuff *
    // ***************
    public bool debugOutput = false;
    private string debugText = "";
    private Rect debugLabeRect = new Rect(20, 500, 400, 200);



	// Use this for initialization
	void Start () {
		
		rig = GetComponent<Rigidbody>();
		if (barrier == null) {
			barrier = GameObject.Find("AsteroidsBarrierForBattleship").transform;
		}
		if (hive == null) {
			hive = GameObject.Find("InsectMothership").transform;
		}
		if (frontPoint == null) {
			frontPoint = GameObject.Find("FrontPoint").transform;
		}
		if (targetPoint == null) {
			targetPoint = GameObject.Find("BattleshipFinalPoint").transform;
		}
		
		time = timerValue;
		
		CheckAsteroids();
	}

	void OnDrawGizmos () {
		if (barrier != null && targetPoint != null) {
			Vector3 stopPos = barrier.transform.position - transform.forward * distanceToAsteroidForStop;
			Vector3 pos = transform.position;
			Vector3 firePos = targetPoint.position;
			Gizmos.color = new Color(0.25f, 0.5f, 1.0f, 1.0f);
			Gizmos.DrawLine(transform.position, stopPos);
			Gizmos.DrawLine(stopPos, firePos);
			Gizmos.color = new Color(1.0f, 0.92f, 0.016f, 0.75f);
			Vector3 pinOffset = new Vector3(0.0f, 10.0f);
			Gizmos.DrawRay(stopPos, pinOffset);
			Gizmos.DrawRay(firePos, pinOffset);
			Gizmos.DrawSphere(stopPos + pinOffset, 2.5f);
			Gizmos.DrawSphere(firePos + pinOffset, 2.5f);
			Gizmos.color = new Color(1.0f, 0.92f, 0.016f, 0.5f);
			Gizmos.DrawWireSphere(barrier.transform.position, 30.0f);
		}
	}


	// Update is called once per frame
	void FixedUpdate () {

        if (asteroidCounter > barrier.transform.childCount)
        {
            CheckAsteroids();
        }

        Vector3 targetVelocity;
        frontZ = frontPoint.position.z;

        if ((asteroidCounter > 0 && frontZ > targetZ + distanceToAsteroidForStop) ||
            (asteroidCounter < 1 && frontZ > targetPoint.position.z))
        {
            targetVelocity = transform.forward * battleshipSpeed;
        }
        else
        {
            targetVelocity = Vector3.zero;
            
            if (asteroidCounter < 1)
            {
                timerActive = true;
            }
        }

        rig.velocity = Vector3.Lerp(rig.velocity, targetVelocity, Time.deltaTime * accelerationFade);

        if (timerActive)
        {
            time -= Time.deltaTime;
        }
		if (time < 0.0f)
        {
            timerActive = false;
			if (!destroyed && hive != null)
            {
				Vector3 fireDirection = hive.transform.position - frontPoint.position;
				mainCannonFire = Instantiate(mainCannonFire, frontPoint.position,
					Quaternion.LookRotation(fireDirection)) as GameObject;
				LineRenderer line = mainCannonFire.GetComponent<LineRenderer>();
				line.SetPosition(0, frontPoint.transform.position);
				line.SetPosition(1, hive.transform.position);
				// Call for team victory and slightly delayed hive detruction:
				Invoke("TimedDestroyHive", hiveExplosionDelay);
                destroyed = true;
            }
       }


	}

	private void TimedDestroyHive () {
		// Spawn a huge ass explosion:
		if (explosion != null) {
			Instantiate(explosion, hive.position, hive.rotation);
		}
		// Destroy the hive:
		if (hive != null) {
			Destroy(hive.gameObject);
		}
		// Destroy instanciated main cannon beam:
		Destroy(mainCannonFire);
		// Declare tech team victory:
		GameMaster.GetLevelMaster().SetPlayerVictory(PlayerFaction.EvilRobots);
	}

    private void CheckAsteroids()
    {
        asteroidCounter = barrier.childCount;

        targetZ = targetPoint.position.z;

        foreach (Transform asteroid in barrier)
        {
            if (asteroid.position.z > targetZ)
            {
                targetZ = asteroid.position.z;
            }
        }
    }


    // ***************
    // * Debug Stuff *
    // ***************
    void OnGUI()
    {
        if (debugOutput)
        {
            debugText = "Anzahl Asteroiden: " + asteroidCounter + "\n";
            debugText += "Nächste Zielposition (z-Achse): " + targetZ.ToString() + "\n";
            debugText += "Position des FrontPoint: " + (int)frontZ + "\n\n\n";

            if (timerActive)
            {
                debugText += "Timer aktiv! Zeit bis Zerstörung: " + (int)time;
            }

            if (destroyed)
            {
                debugText += "Hive zerstört!";
            }

            GUI.Label(debugLabeRect, debugText);
        }
    }

}
