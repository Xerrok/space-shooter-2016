﻿using UnityEngine;
using System.Collections;

public enum TurretTargetingMethod {
	ShortestDistanceToTarget,
	SmallestAngleToTarget,
	SmallestLateralVelocity,
	RandomIGuess
}

public class Turret : MonoBehaviour {

	/// <summary>
	/// Force the use of a target prediction algorithm for this autonomous turret.
	/// </summary>
	public bool useTargetPrediction = false;
	public TurretTargetingMethod targetingMode = TurretTargetingMethod.ShortestDistanceToTarget;

	public string enemyTag = "Player";

	/// <summary>
	/// The target transform this turret is engaging right now.
	/// </summary>
	public Transform target = null;

	/// <summary>
	/// The position in world space this turret is aiming at (target prediction applied on target).
	/// </summary>
	private Vector3 targetPos;

	/// <summary>
	/// The weapon barrel on this turret, aka the actual rotating thingy.
	/// </summary>
	public Transform barrel = null;

	/// <summary>
	/// The actual weapon script responsible for filling the air with plenty o'lead.
	/// </summary>
	public WeaponLaser gun = null;

	/// <summary>
	/// Rotation speed of this turret's weapon platform.
	/// </summary>
	public float rotationSpeed = 3.0f;

	/// <summary>
	/// The minimum angle between the barrel and the target before opening fire. (in degrees)
	/// </summary>
	public float minFireAngle = 5.0f;

	/// <summary>
	/// The maximum distance to target before opening fire at it.
	/// </summary>
	public float maxFireRange = 350.0f;

	/// <summary>
	/// Average speed of the projectiles fired by this turret. (in m/s)
	/// </summary>
	private float invProjectileSpeed = 1.0f;


	// Use this for initialization
	void Start () {
		if (gun != null) {
			invProjectileSpeed = 1.0f / gun.projectileSpeed;
		}
	}

	void OnDrawGizmos () {
		Gizmos.color = new Color(1.0f, 0.2f, 0.02f, 0.33f);
		Gizmos.DrawWireSphere(transform.position, 2.5f);
	}

	/// <summary>
	/// Estimate the target object's velocity in world space.
	/// </summary>
	/// <returns>The target velocity.</returns>
	private Vector3 GetTargetVelocity (Transform tar) {
		// Just use rigidbody velocity for simpicity's sake:
		Rigidbody rig = tar.GetComponent<Rigidbody>();
		// Return dat cr*p:
		return (rig != null) ? rig.velocity : Vector3.zero;
	}

	/// <summary>
	/// Gets the target's approximated/predicted location by the time the projectiles reach it.
	/// </summary>
	/// <returns>A prediction on the target's position based on projectile velocity.</returns>
	/// <param name="trans">The target game object's transform we're aiming for.</param>
	private Vector3 GetTargetPrediction () {
		// Calculate the time the projectile needs to reach the target's current location:
		float distance = Vector3.Distance(target.position, transform.position);
		float flightTime = distance * invProjectileSpeed;

		// Retrieve movement speed of the target object:
		Vector3 targetVelocity = GetTargetVelocity(target);
		targetVelocity *= 1.0f + Mathf.Abs(Vector3.Dot(transform.forward, targetVelocity.normalized));

		// Add the estimated trajectory progression to the current position and return it:
		return target.position + targetVelocity * flightTime;
	}

	/// <summary>
	/// Finds a new target GO in scene.
	/// </summary>
	/// <returns>The new target according to targeting algorithm.</returns>
	private Transform FindNewTarget () {
		Transform tar = null;
		GameObject[] targets = GameObject.FindGameObjectsWithTag(enemyTag);

		// SHORTEST DISTANCE TO TARGET:
		if (targetingMode == TurretTargetingMethod.ShortestDistanceToTarget) {
			float shortestDist = 100000.0f;

			for (int i = 0; i<targets.Length; i++) {
				if (targets[i] != null) {
					float tDist = Vector3.Distance(transform.position, targets[i].transform.position);
					if (tDist < shortestDist && tDist < maxFireRange) {
						tar = targets[i].transform;
						shortestDist = tDist;
					}
				}
			}
		}

		// SMALLEST ROTATION ANGLE TO TARGET:
		else if (targetingMode == TurretTargetingMethod.SmallestAngleToTarget) {
			float smallestAngle = 180.0f;

			for (int i = 0; i<targets.Length; i++) {
				if (targets[i] != null) {
					Vector3 targetDir = targets[i].transform.position - transform.position;
					if (targetDir.magnitude < maxFireRange) {
						float tAng = Vector3.Angle(transform.forward, targetDir);
						if (tAng < smallestAngle) {
							tar = targets[i].transform;
							smallestAngle = tAng;
						}
					}
				}
			}
		}

		// SMALLEST ROTATION ANGLE TO TARGET:
		else if (targetingMode == TurretTargetingMethod.SmallestLateralVelocity) {
			float smallestLateral = 100000.0f;
			Vector3 planarScaler = Vector3.one - transform.forward;

			for (int i = 0; i<targets.Length; i++) {
				if (targets[i] != null && Vector3.Distance(transform.position, targets[i].transform.position) < maxFireRange) {
					Vector3 latVel = Vector3.Scale(GetTargetVelocity(targets[i].transform), planarScaler);
					float latSpeed = latVel.magnitude;
					if (latSpeed < smallestLateral) {
						tar = targets[i].transform;
						smallestLateral = latSpeed;
					}
				}
			}
		}

		// RANDOM TARGET PICKING:
		else {
			tar = targets[Random.Range(0, targets.Length - 1)].transform;
		}

		return tar;
	}

	// Update is called once per frame
	void Update () {
		// Find a new target if none is assigned:
		if (target == null) {
			target = FindNewTarget();
		}
		// If we already have a target and an active weapon is assigned:
		if (target != null && gun != null) {
			// Get target position:
			targetPos = (useTargetPrediction == true) ? GetTargetPrediction() : target.position;

			// Calculate target direction and deduce target rotation:
			Vector3 targetDirection = targetPos - gun.transform.position;
			Quaternion targetRotation = Quaternion.LookRotation(targetDirection);

			// Rotate the main cannon into firing position:
			barrel.rotation = Quaternion.Slerp(barrel.rotation, targetRotation, Time.deltaTime * rotationSpeed);

			// If the target is within firing range:
			if (targetDirection.sqrMagnitude < maxFireRange * maxFireRange) {

				// If the barrel is pointed at the target object:
				if (Vector3.Angle(barrel.forward, targetDirection) < minFireAngle) {
					// Open fire, let them have it, fill the air with lead, shred through armor.
					// Witness the enemy's demise as he it torn to bits by volley after volley of airborn shrapnel!
					// For one day, the skies shall be free once more, cleansed of them warmoning peasants.
					// Prepare for death, prepare for glory! Destroy our enemies, oh glorious gun turret.
					// For thine might is great, thou shalt be feared.
					// Fear the majestic gun turret, fear and shudder in awe.
					// All hail the eternal rise of autonomous warfare.
					// Amen & Attack.
					// ... No, I have nothing better to do.
//					Debug.Log("Verdammtes Teil hat gefeuert:  " + Time.time);
					gun.Fire(1.0f);
				}
			}
			// Reset target if it is outside the turret's reach:
			else {
				target = null;
			}
		}
	}
}
