﻿using UnityEngine;
using System.Collections;

public class DockController : MonoBehaviour {

    // Docks 
    private Transform[] docks = null;

    // Dock-Belegung (Player ID / NPC ID)
    private GameObject[] dockAssignment = null;
    private int docksAvailable = 0;
    
    // Warteschlange
    private Queue launchingQueue = new Queue();


    /// <summary>
    /// 
    /// </summary>
	void Awake () {

        if (transform.childCount > 0)
        {            
            docksAvailable = transform.childCount;
            docks = new Transform[docksAvailable];
            dockAssignment = new GameObject[docksAvailable];


            for (int i = 0; i < docks.Length; i++)
            {
                docks[i] = transform.GetChild(i);
                dockAssignment[i] = null;
            }
        }
        else
        {
            Debug.LogError("Spawning points not available");
        }
    }
	
    /// <summary>
    /// 
    /// </summary>
    /// <param name="_player"></param>
    private void AddToLaunchingQueue(GameObject _player)
    {
        launchingQueue.Enqueue(_player);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="_player"></param>
    private void MoveShipToDock(GameObject _player)
    {
        if (docksAvailable > 0)
        {
            dockAssignment[docksAvailable-1] = _player;
            _player.GetComponent<PlayerController>().SetSpawnPoint(docks[docksAvailable-1]);
            
            docksAvailable--;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="_player"></param>
    public void RequestLaunchingPlace(GameObject _player)
    {
        if (launchingQueue.Count > 0 || docksAvailable < 1)
        {
            AddToLaunchingQueue(_player);
//            Debug.Log("Player " + _player.GetComponent<PlayerController>().GetPlayerIndex() + " wurde der dockingQueue hinzugefügt.");
        }
        else
        {
            MoveShipToDock(_player);
//            Debug.Log("Player " + _player.GetComponent<PlayerController>().GetPlayerIndex() + ": Startprozedur einleiten!");
        }
    }
}
