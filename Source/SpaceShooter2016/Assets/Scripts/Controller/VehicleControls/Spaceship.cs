﻿using UnityEngine;
using System.Collections;

public enum EShipFlightMode {
	Normal,
	Maneuver,
	DockingLaunch,
	DockingReturn,
	CourseCorrection
}
public enum EShipManeuver {
	BarrelRollL,
	BarrelRollR,
	ImmelmannTurn,
	SplitSTurn
}

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ItemUpgrade))]
[AddComponentMenu("Scripts/Vehicles/Spaceship")]
public class Spaceship : Vehicle {

	// SHIP FLIGHT STATUS:

	/// <summary>
	/// The flight mode the ship is currently in.
	/// </summary>
	public EShipFlightMode flightMode = EShipFlightMode.Normal;
	/// <summary>
	/// Which maneuver is currently being performed. (If in maneuver flight mode)
	/// </summary>
	private EShipManeuver maneuver = EShipManeuver.BarrelRollL;

	// GENERAL FLIGHT:

	/// <summary>
	/// The speed the spacecraft is currently supposed to fly forward at. (in m/s)
	/// </summary>
	private float targetSpeed = 0.0f;
	/// <summary>
	/// The minimal desired forward flight, aka sloth mode. (in m/s)
	/// </summary>
	public float minSpeed = 10.0f;
	/// <summary>
	/// The maximum normal forward flight speed of the spacecraft. (in m/s)
	/// </summary>
	public float maxSpeed = 60.0f;
	/// <summary>
	/// The maximum speed attainable through the excessive use of boost and other anesthetics.
	/// </summary>
	public float boostSpeed = 100.0f;
	/// <summary>
	/// The speed lost per second due to our awesomely fictional friction force - in friggin' space! (in m/s^2)
	/// </summary>
	public float frictionRate = 1.0f;
	/// <summary>
	/// How much speed is gained per second when accelerating. (in m/s^2)
	/// </summary>
	public float accelerationRate = 3.0f;
	/// <summary>
	/// The speed at which the ship's velocity can go from 0 to 100%. (Lower = drifting, Higher = hard turns)
	/// </summary>
	public float accelerationFade = 1.7f;
	public float boostFuel = 0.0f;
	public float maxFuel = 100.0f;
	/// <summary>
	/// The rot acceleration.
	/// </summary>
	public float rotAcceleration = 3.0f;
	/// <summary>
	/// The input axis as they are issued by the 'control', aka what way the ship's turning right now.
	/// </summary>
	private Vector3 inputAxis;
	/// <summary>
	/// What rotation the ship is supposed to have right now. (euler angles)
	/// </summary>
	private Vector3 rotation = new Vector3();
	/// <summary>
	/// Rotation speed along all three local rotation axis. (in degree/s)
	/// </summary>
	public Vector3 rotationSpeed = new Vector3(1.5f, 2.0f, 4.0f);
	/// <summary>
	/// The maximum upwards facing angle, aka the maximum pitch deviation from horizontal plane.
	/// </summary>
//	public float pitchLimit = 45.0f;

	public float yawRollingAngle = 45.0f;
	private float curRollAngle = 0.0f;
	public Transform rollChild = null;

	// MANEUVERS:

	/// <summary>
	/// The rotation speed along the ship's axis, aka the roll speed during maneuvers.
	/// </summary>
	public float maneuverRollSpeed = 130.0f;
	/// <summary>
	/// The rotation speed along the ship's sideways axis, aka the pitch speed during maneuvers.
	/// </summary>
	public float maneuverPitchSpeed = 110.0f;

    public float maneuverCoolDown = 5.0f;
    private bool mCoolDownActive = false;
    private float mCoolDownTime = 0.0f;
	/// <summary>
	/// How many degrees the ship rotated around it's own axis already.
	/// </summary>
	private float rollAmount = 0.0f;
	/// <summary>
	/// How many degrees the ship rotated around it's sideways axis already.
	/// </summary>
	private float pitchAmount = 0.0f;
	/// <summary>
	/// The total number of rolls to perform during a barrel roll maneuver.
	/// </summary>
	public int barrelRolls = 1;
	/// <summary>
	/// The sideways drifting speed while performing the maneuver. (influences radius of the roll circle)
	/// </summary>
	public float barrelRollDrift = 5.0f;

	// DOCKING PROCEDURES:

	/// <summary>
	/// What is the minimal approach angle for a successful docking start. (in degrees)
	/// </summary>
	public float dockApproachAngle = 35.0f;
	/// <summary>
	/// The maximum distance to docking port for successful approach run. (Closer or it won't work!)
	/// </summary>
	public float dockApproachRange = 50.0f;
	/// <summary>
	/// Any docking port this vehicle is currently docked at, null if not docked.
	/// </summary>
	private Transform dockedAtPort = null;
    /// <summary>
    /// Time in seconds for launching mode is active
    /// </summary>
	public float dockTimerMin = 5.0f;
    /// <summary>
    /// Launching timer active?
    /// </summary>
    private bool dockTimerActive = false;
    /// <summary>
    /// 
    /// </summary>
    private float dockTime = 0.0f;
    /// <summary>
    /// Vehicle speed in launching mode
    /// </summary>
    public float dockLaunchSpeed = 20.0f;

	private bool spawningAtRobotMothership = false;
    private Rigidbody followRobotMothership;

	// COURSE CORRECTION:

    /// <summary>
    /// The target forward vector for course correction
    /// </summary>
    private Vector3 targetCourse = Vector3.zero;


	// Use this for initialization
	void Start () {
		// Get the rigidbody physics component responsible for ship movement:
		rig = GetComponent<Rigidbody>();
		// Tell the rigidbody to ignore gravity:
		rig.useGravity = false;
		// Get the initial rotation of the ship:
		rotation = transform.eulerAngles;
		rotation.z = 0.0f;
		// Initialise weapon systems:
		if (weaponSystem != null) {
			AssignNewWeaponSystem(weaponSystem);
		}
		// Get the upgrade system on the ship:
		upgradeSystem = GetComponent<ItemUpgrade>();

		if (control.faction == PlayerFaction.EvilRobots) {
			followRobotMothership = GameObject.Find("Mothership").GetComponent<Rigidbody>();
		}
	}

	/// <summary>
	/// Assigns a new controller from either a player or an AI to this spacecraft/vehicle.
	/// </summary>
	/// <param name="newCtrl">The new controller we want to assign.</param>
	public override void AssignNewController (Controller newCtrl) {
		if (newCtrl != null) {
			control = newCtrl;
		}
	}


	/// <summary>
	/// Request the spacecraft to perform either of it's maneuvers if possible.
	/// </summary>
	/// <returns><c>true</c>, if the desired maneuver could be started, <c>false</c> if unavailable.</returns>
	/// <param name="newManeuver">Which maneuver to perform.</param>
	public override bool StartManeuver (EShipManeuver newManeuver = EShipManeuver.BarrelRollL) {
		// Only allow maneuvers if we're in normal flight mode:
		if (flightMode == EShipFlightMode.Normal && !mCoolDownActive) {
			// Preemptively reset roll and pitch measuring:
			rollAmount = 0.0f;
			pitchAmount = 0.0f;

			// Start maneuver:
			// [TODO: Insert check for maneuver cooldown/availability here!]
			flightMode = EShipFlightMode.Maneuver;
			maneuver = newManeuver;

//			Debug.Log("Starting maneuver:  " + flightMode.ToString() + "    (" + Time.time + ")");

			// Return true if maneuver could be started:
			return true;
		}
		// Maneuver is currently unavailable, return false:
		return false;
	}


	/// <summary>
    /// Starts a docking docking sequence such as launching from or landing at mothership.
    /// </summary>
    /// <returns><c>true</c>, if mode could be started, <c>false</c> if not.</returns>
    public override bool StartDocking()
    {
        flightMode = EShipFlightMode.DockingLaunch;

        dockTimerActive = true;
        dockTime = dockTimerMin;

        (control as PlayerController).playerInterface.launchModeActive.gameObject.SetActive(true);

        if ((control as PlayerController).faction == PlayerFaction.EvilRobots)
        {
            spawningAtRobotMothership = true;
        }

        GetComponent<BoxCollider>().isTrigger = true;

//        Debug.Log("DockingLaunchModus gesetzt");

        return true;
    }


	// Fixed Update is called at set timed intervals
	void FixedUpdate () {
		// Verify that both control and rigidbody physics are assigned and non null:
		if (control != null && rig != null) {
			// Fetch input from controller:
			inputAxis = control.inputAxis;

			// Normal manual flight mode:
			if (flightMode == EShipFlightMode.Normal) {
				UpdateNormalMode();
			}
			// Fly maneuvers:
			else if (flightMode == EShipFlightMode.Maneuver) {
				UpdatemaneuverMode();
			}
			// Automatic docking sequences on mothership:
			else if (flightMode == EShipFlightMode.DockingLaunch) {
				UpdateDockingMode();
			}
			// Automatic course correction:
            else if (flightMode == EShipFlightMode.CourseCorrection)
            {
                UpdateCourseCorrection();
            }
		}

        // ManuverCoolDown Timer
        if (mCoolDownActive)
        {
            mCoolDownTime -= Time.deltaTime;
            float fillAmountInvisible = mCoolDownTime / maneuverCoolDown;
            (control as PlayerController).playerInterface.maneuverIconCooldown.fillAmount = fillAmountInvisible;
            if (mCoolDownTime < 0.0f)
            {
                mCoolDownActive = false;
                (control as PlayerController).playerInterface.maneuverIcon.gameObject.SetActive(false);
                (control as PlayerController).playerInterface.maneuverIconCooldown.gameObject.SetActive(false);
            }
        }

        if (weaponSystem != null && weaponSystem.mine != null) {
			control.isCarryingMine = (weaponSystem.mine.ammo > 0);
        }
	}

	// NORMAL MODE:

	/// <summary>
	/// Update method for when the ship is in regular manual flight mode.
	/// </summary>
	private void UpdateNormalMode () {
		// Get the vehicle's current velocity:
		Vector3 currentVel = rig.velocity;

		// SHIP SPEED:

		float topSpeed = maxSpeed;
		// Double acceleration speed when boosting:
		float boostRate = 1.0f;
		if (control.boost == true) {
			boostFuel = Mathf.Max(0.0f, boostFuel - Time.deltaTime * 10.0f);
			// Double acceleration rate and use a higher top speed when boosting:
			if (boostFuel > 0.0f) {
				boostRate = 2.0f;
				topSpeed = boostSpeed;
			}
		}
		float accelRate = 0.0f;
		if (control.acceleration != 0.0f) {
			accelRate = (control.acceleration + control.deceleration) * accelerationRate * Time.deltaTime;
		}
		else {
			accelRate = -frictionRate * Time.deltaTime;
		}
		// Update vehicle speed:
		targetSpeed = Mathf.Clamp(targetSpeed + boostRate * accelRate, minSpeed, topSpeed);
		// Draw velocity vector from ship heading and target speed:
		// (Slow down ship if it's carrying a mine.)
		Vector3 targetVel = transform.forward * targetSpeed;

		// Apply new velocity to rigidbody physics:
		rig.velocity = Vector3.Lerp(currentVel, targetVel, Time.deltaTime * accelerationFade);

		// SHIP ROTATION:

/*
		// Project ship's forward direction onto the horizontal plane:
		float forwardPitchAngle = Vector3.Angle(Vector3.up, transform.forward);
		// Check wether the angle between ship heading and horizontal plane exceed pitch limitations:
		bool pitchExceeded = (inputAxis.x < 0.0f) ?
			(forwardPitchAngle < 90.0f - pitchLimit) :
			(forwardPitchAngle > 90.0f + pitchLimit);	// (Yeah, this part is a huge mess...)

		// Limit ship's maximum and minimum pitch angle:
		float pitchLimitFactor = (pitchExceeded == false) ? 1.0f : 0.0f;
*/

		// Assemble ship rotation axis and angles:
		Vector3 targetAngularVel = Vector3.up * inputAxis.y * rotationSpeed.y
			+ transform.forward * Vector3.Dot(Vector3.down, transform.right) * rotationSpeed.z
			+ transform.right * inputAxis.x * rotationSpeed.x;// * pitchLimitFactor;
		// Interpolate and apply angular speed to vehicle:
		rig.angularVelocity = Vector3.Lerp(rig.angularVelocity, targetAngularVel, rotAcceleration * Time.deltaTime);

		// ADDITIONAL CHILD ROLL:

		if (rollChild != null) {
			float tarRollAngle = -yawRollingAngle * inputAxis.y;
			curRollAngle = Mathf.Lerp(curRollAngle, tarRollAngle, Time.deltaTime * 2.0f);
			rollChild.localEulerAngles = new Vector3(0.0f, 0.0f, curRollAngle);
		}

	}


	// MANEUVERS:

	/// <summary>
	/// Update method for when the ship is performing a maneuver.
	/// </summary>
	private void UpdatemaneuverMode () {
		Vector3 targetVel = transform.forward * maxSpeed;

		// BARREL ROLL left:
		if (maneuver == EShipManeuver.BarrelRollL) {
			targetVel = DoBarrelRoll(targetVel, 1.0f);
		}
		// BARREL ROLL right:
		else if (maneuver == EShipManeuver.BarrelRollR) {
			targetVel = DoBarrelRoll(targetVel, -1.0f);
		}

		// IMMELMANN TURN:
		else if (maneuver == EShipManeuver.ImmelmannTurn) {
			DoImmelmannTurn();
		}
		// SPLIT-S TURN: (= inverse Immelmann)
		else if (maneuver == EShipManeuver.SplitSTurn) {
			DoSplitSTurn();
		}

		// Update velocity:
		rig.velocity = Vector3.Lerp(rig.velocity, targetVel, Time.deltaTime * accelerationFade);
	}

	// Barrel roll: Roll the ship while drifting sideways.
	private Vector3 DoBarrelRoll (Vector3 targetVel, float direction = 1.0f) {
		// Add a lateral drift to the ship's velocity:
		targetVel += transform.right * barrelRollDrift * direction;
		// Make sure we know how far we turned so far:
		float barrelRollAmountAdd = maneuverRollSpeed * Time.deltaTime;
		rollAmount += barrelRollAmountAdd;
		// Rotate the ship along it's longitudinal axis (local z-axis):
		rig.angularVelocity = transform.forward * direction * barrelRollAmountAdd;

		// Stop the maneuver if we made enough turns:
		if (rollAmount >= 320.0f * (float)barrelRolls) {
			// Set next flight mode:
			flightMode = EShipFlightMode.Normal;
			// Reset barrel roll parameters:
			rollAmount = 0.0f;
            StartManeuverCoolDown();
        }

		return targetVel;
	}
	// Immelmann turn: Half a looping followed by half a roll.
	private void DoImmelmannTurn () {
		Vector3 tarRotSpeed = new Vector3();
		// 1. Perform half a looping: (aka do a 180° turn)
		if (pitchAmount < 180.0f) {
			// Remember how far the ship turned so far:
			float pitchAmountAdd = maneuverPitchSpeed * Time.deltaTime;
			pitchAmount += pitchAmountAdd;
			// Then rotate it along it's local x-axis (pitch):
			tarRotSpeed = transform.right * -pitchAmountAdd;
		}
		// After the looping part is over:
		else {
			// 2. Perform half a spin along the ship's axis: (aka another 180° turn)
			if (rollAmount < 100.0f) {
				// Remember how far the ship turned so far:
				float immelmannRollAmountAdd = maneuverRollSpeed * Time.deltaTime;
				rollAmount += immelmannRollAmountAdd;
				// Rotate the ship:
				tarRotSpeed = transform.forward * immelmannRollAmountAdd;
			}
			// 3. Complete maneuver:
			else {
				// Set next flight mode:
				flightMode = EShipFlightMode.Normal;
				// Reset the Immelmann turn parameters:
				rollAmount = 0.0f;
				pitchAmount = 0.0f;
				// Update target rotation:
				rotation = Vector3.Scale(transform.eulerAngles, Vector3.up);                
                StartManeuverCoolDown();
			}
		}
		rig.angularVelocity = Vector3.Lerp(rig.angularVelocity, tarRotSpeed, Time.deltaTime * 3.0f);
	}
	// Split-S turn: Half a roll followed by half a looping.
	private void DoSplitSTurn () {
		Vector3 tarRotSpeed = new Vector3();
		// 1. Perform half a spin along the ship's axis: (aka another 180° turn)
		if (rollAmount < 180.0f) {
			// Remember how far the ship turned so far:
			float immelmannRollAmountAdd = maneuverRollSpeed * Time.deltaTime;
			rollAmount += immelmannRollAmountAdd;
			// Rotate the ship:
			tarRotSpeed = transform.forward * immelmannRollAmountAdd;
		}
		// After the looping part is over:
		else {
			// 1. Perform half a looping: (aka do a 180° turn)
			if (pitchAmount < 160.0f) {
				// Remember how far the ship turned so far:
				float pitchAmountAdd = maneuverPitchSpeed * Time.deltaTime;
				pitchAmount += pitchAmountAdd;
				// Then rotate it along it's local x-axis (pitch):
				tarRotSpeed = transform.right * -pitchAmountAdd;
			}
			// 3. Complete maneuver:
			else {
				// Set next flight mode:
				flightMode = EShipFlightMode.Normal;
				// Reset the Immelmann turn parameters:
				rollAmount = 0.0f;
				pitchAmount = 0.0f;
				// Update target rotation:
				rotation = Vector3.Scale(transform.eulerAngles, Vector3.up);
                StartManeuverCoolDown();
            }
		}
		rig.angularVelocity = Vector3.Lerp(rig.angularVelocity, tarRotSpeed, Time.deltaTime * 3.0f);
	}


	// COURSE CORRECTION:

    private void UpdateCourseCorrection()
    {
        Vector3 targetVel = transform.forward * maxSpeed;

        // calculate target direction
        Quaternion targetDirection = Quaternion.LookRotation(targetCourse, Vector3.up);

        // Update rotation:
        rig.rotation = Quaternion.Slerp(rig.rotation, targetDirection, Time.deltaTime * 2.0f);
        
        // Update velocity:
        rig.velocity = Vector3.Lerp(rig.velocity, targetVel, Time.deltaTime * accelerationFade);
    }


	// DOCKING:

	/// <summary>
	/// Update method for when the ship performing an automatic docking maneuver.
	/// </summary>
	private void UpdateDockingMode () {

		// DockingTimer & VehicleSpeed mechanic 
        if (dockTimerActive)
        {
            Vector3 targetVel = transform.forward * dockLaunchSpeed;

            if (followRobotMothership != null && spawningAtRobotMothership)
            {
                targetVel += followRobotMothership.velocity;                
            }

            // Apply new velocity to rigidbody physics:
            rig.velocity = targetVel;
            // rig.velocity = Vector3.Lerp(rig.velocity, targetVel, Time.deltaTime * accelerationFade);

            // Nach ablauf des Timers auf normalen flight Modus wechseln
            dockTime -= Time.deltaTime;

            if (dockTime < 0.0f)
            {
                dockTimerActive = false;
                targetSpeed = dockLaunchSpeed;
                flightMode = EShipFlightMode.Normal;
                (control as PlayerController).playerInterface.launchModeActive.gameObject.SetActive(false);
                GetComponent<BoxCollider>().isTrigger = false;

//                Debug.Log("DockingLaunchModus beendet!");
            }
        }
	}


	// GETTER:

    /// <summary>
    /// Return the ShipFlightMode
    /// </summary>
    /// <returns></returns>
    public EShipFlightMode GetFlightMode()
    {
        return this.flightMode;
    }

    /// <summary>
    /// Return the ShipManeuver 
    /// </summary>
    /// <returns></returns>
    public EShipManeuver GetManeuver()
    {
        return this.maneuver;
    }

	/// <summary>
    /// Starts a course correction sequence (e.g. on reaching environment boundary) 
    /// </summary>
    /// <returns></returns>
    public override bool StartCourseCorrection(Vector3 newForwardCourse)
    {
        if (newForwardCourse != Vector3.zero)
        {
            targetCourse = newForwardCourse;
            flightMode = EShipFlightMode.CourseCorrection;
        }
        return false;
    }

    /// <summary>
    /// Stop the course correction sequence
    /// </summary>
    /// <returns></returns>
    public override bool StopCourseCorrection()
    {
        flightMode = EShipFlightMode.Normal;
        return false;
    }

    private void StartManeuverCoolDown()
    {
        // Set maneuver cooldown time 
        mCoolDownTime = maneuverCoolDown;
        // Activate maneuver cooldown 
        mCoolDownActive = true;

        (control as PlayerController).playerInterface.maneuverIcon.gameObject.SetActive(true);
        (control as PlayerController).playerInterface.maneuverIconCooldown.gameObject.SetActive(true);
    }
}
