﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(RaycastForObstacles))]
[RequireComponent(typeof(DamageCalc))]
public abstract class Vehicle : MonoBehaviour {

	/// <summary>
	/// The controller transmitting any input commands from either player or AI.
	/// </summary>
	public Controller control;

	/// <summary>
	/// The weapon system installed on this vehicle, null if no weapons onboard.
	/// </summary>
	public WeaponSystem weaponSystem;

	/// <summary>
	/// The upgrade system influencing this vehicle, aka the thingy that makes you pick up items.
	/// </summary>
	protected ItemUpgrade upgradeSystem;

	/// <summary>
	/// The rigidbody physics component of this spacecraft, required for collisions and movement.
	/// </summary>
	protected Rigidbody rig = null;

	/// <summary>
	/// Any set of debuffs acting on this vehicle.
	/// </summary>
	public Debuffs debuffs;

	/// <summary>
	/// Assigns a new controller from either a player or an AI to this spacecraft/vehicle.
	/// </summary>
	/// <param name="newCtrl">The new controller we want to assign.</param>
	public virtual void AssignNewController (Controller newCtrl) {
		if (newCtrl != null) {
			control = newCtrl;
		}
	}

	/// <summary>
	/// Assigns a new weapon system to a vehicle or turret.
	/// </summary>
	/// <param name="newWeaponSystem">The weapon system we want to assign to this vehicle or turret.</param>
	public void AssignNewWeaponSystem (WeaponSystem newWeaponSystem) {
		// Ignore null requests and don't assign a same weapon system a second time:
		if (newWeaponSystem != null && weaponSystem != newWeaponSystem) {
			// If we already have another weapon system up and running:
			if (weaponSystem != null) {
				// Destroy existing one and replace it:
				Destroy(weaponSystem.gameObject);
			}
			// Assign new weapon system:
			weaponSystem = newWeaponSystem;
			// Tell the new weapon system that this vehicle/turret is the one controlling it:
			weaponSystem.AssignNewVehicle(this);
		}
	}

	/// <summary>
	/// Apply a new set of debuffs to this player's vehicle.
	/// </summary>
	/// <param name="newDebuffs">A set of debuffs.</param>
	public void AddPlayerDebuff (Debuffs newDebuffs) {
		if (control != null) {
			debuffs = newDebuffs;
		}
	}

	/// <summary>
	/// Request the spacecraft to perform either of it's maneuvers if possible.
	/// </summary>
	/// <returns><c>true</c>, if the desired maneuver could be started, <c>false</c> if unavailable.</returns>
	/// <param name="newManeuver">Which maneuver to perform.</param>
	public virtual bool StartManeuver (EShipManeuver newManeuver = EShipManeuver.BarrelRollL) {
		return false;
	}


    /// <summary>
    /// Starts a docking docking sequence such as launching from or landing at mothership.
    /// </summary>
    /// <returns><c>true</c>, if mode could be started, <c>false</c> if not.</returns>
    public virtual bool StartDocking()
    {
        return false;
    }


	/// <summary>
    /// Starts a course correction sequence (e.g. on reaching environment boundary) 
    /// </summary>
    /// <returns></returns>
    public virtual bool StartCourseCorrection(Vector3 newForwardCourse)
    {
        return false;
    }

    /// <summary>
    /// Stop the course correction sequence
    /// </summary>
    /// <returns></returns>
    public virtual bool StopCourseCorrection()
    {
        return false;
    }

}
