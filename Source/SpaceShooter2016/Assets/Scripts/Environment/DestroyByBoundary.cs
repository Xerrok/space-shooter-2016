﻿using UnityEngine;
using System.Collections;

public class DestroyByBoundary : MonoBehaviour
{
	void OnTriggerExit (Collider other)
    {
        if (other.gameObject.CompareTag("RocketProjectile"))
        {
            other.gameObject.SendMessage("ApplyDamage", 10000.0f, SendMessageOptions.DontRequireReceiver);
        }
    }
}
