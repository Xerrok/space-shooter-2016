﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class DestroyParticleSystem : NetworkBehaviour
{
    private ParticleSystem ps;

    void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        if (ps != null)
        {
            if (!ps.IsAlive())
            {
                NetworkServer.Destroy(gameObject);
            }
        }
    }
}
