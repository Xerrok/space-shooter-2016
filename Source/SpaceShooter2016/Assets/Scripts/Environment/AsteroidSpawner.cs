﻿using UnityEngine;
using System.Collections;

public class AsteroidSpawner : MonoBehaviour
{
    public GameObject asteroidObject;
    public float maxObjectSize;
    public Vector3 spawnPosition;
    public int spawnCount;

    public float firstSpawnTime;
    public float spawnDelay;
    public float spawnTime;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(SpawnWaves());
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(firstSpawnTime);

        while (true)
        {
            for (int i = 0; i < spawnCount; i++)
            {
                Vector3 spawnPos = new Vector3(Random.Range(-spawnPosition.x, spawnPosition.x), spawnPosition.y, Random.Range(-spawnPosition.z, spawnPosition.z));
                Quaternion spawnRot = Quaternion.identity;
                Instantiate(asteroidObject, spawnPos, spawnRot);

                yield return new WaitForSeconds(spawnDelay);
            }

            yield return new WaitForSeconds(spawnTime);
        }
    }
}
