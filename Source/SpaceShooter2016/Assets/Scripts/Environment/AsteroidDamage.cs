﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class AsteroidDamage : MonoBehaviour
{

	public Damage damage = new Damage();
	public GameObject explosion;
	private List<Collider> damageList = new List<Collider>();


	void OnCollisionEnter(Collision _col){
		_col.gameObject.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
//		Debug.Log ("Ich editiere das Level erst wenn das Game fertig ist! -Gerhard 2016 ");
		// Das freut mich, Gerry...
	}
}