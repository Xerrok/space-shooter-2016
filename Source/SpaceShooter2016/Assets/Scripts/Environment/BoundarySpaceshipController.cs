﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Enumeration, which holds triggering actions for BoundarySpaceshipController
/// </summary>
public enum EBoundaryAction
{
    Destroy,
    Autopilot
}

public class BoundarySpaceshipController : MonoBehaviour {

    /// <summary>
    /// Action, which will triggered at the final boundary
    /// </summary>
    public EBoundaryAction boundaryAction = EBoundaryAction.Autopilot;

    // GUI Stuff

    /// <summary>
    /// Use Unity GUI for output? 
    /// </summary>
    public bool useGUIOutput = true;
    
    /// <summary>
    /// Position for GUI elements, relative to game screen center
    /// </summary>
    public Vector2 outputPosition = new Vector2(0,0);

    // calculated game screen center
    private Vector2 centerPosition = Vector2.zero;
    // size vector for GUI labels 
    private Vector2 labelSize = new Vector2(400, 100);
    // rectangle for GUI label 1
    private Rect labelRect1 = new Rect();
    // rectangle for GUI label 2
    private Rect labelRect2 = new Rect();
    // text color
    private Color textColor1 = new Color(1, 0, 0, 0);
    // text 1
    private string Text1 = "WARNING!";
    // text 2 
    private string Text2 = "";
    // GUI Style for label 1
    private GUIStyle style1 = new GUIStyle();
    // GUI Style for label 2
    private GUIStyle style2 = new GUIStyle();

    // calculated alpha value for text color
    private float alpha = 0.0f;
    // this value will added to alpha value for text color
    private float alphaAdd = 0.0f;


    // outside of the first boundary?
    private bool outsideBoundary = false;
    // transform component from first boundary object
    private Transform boundary;
    // local scale values from first boundary object
    private Vector3 boundaryScale;
    // local scale values from final boundary object
    private Vector3 boundaryScaleForAction;
    // direction for autopilot action
    private Vector3 autopilotDirection = Vector3.zero;
    // save boundary differences for x, y, z axis
    private float[] boundaryDiff = new float[3];
    // calculated distance units for text output (smallest value from 3 axis) 
    private float unitsToTriggerAction = 0.0f;

    // ship position 
    private Vector3 shipPosition;
    // warning distance for x, y, z axis
    private Vector3 warningDistance;

    private Spaceship shipControl;
    private Camera cam;


	// Use this for initialization
	void Start() 
    {
        // Get transform from first boundary object
        boundary = GameObject.Find("BoundaryForWarning").transform;
        // Save local scale values from first boundarys transform
        boundaryScale = boundary.localScale;
        // Get local scale values from final boundary object
        boundaryScaleForAction = GameObject.Find("Boundary").transform.localScale;
        // Calculate the distance between first boundary and final boundary for the warning message
        warningDistance = (boundaryScaleForAction - boundaryScale) / 2;

        
        shipControl = gameObject.GetComponent<Spaceship>();

        cam = shipControl.control.gameObject.GetComponent<Camera>();        

        centerPosition = cam.pixelRect.center;

        // y Werte umrechnen! Kamerakoordinaten fangen unten links an!
        centerPosition.y = Screen.height - centerPosition.y;
        

        // Set GUI Style 1
        style1.alignment = TextAnchor.MiddleCenter;
        style1.fontSize = 24;
        style1.fontStyle = FontStyle.Bold;
        style1.normal.textColor = textColor1;
        labelRect1.size = labelSize;
        labelRect1.position = (centerPosition + outputPosition) - labelRect1.center;
        // Debug.Log(labelRect1.center.ToString());

        // Set GUI Style 2
        style2.alignment = TextAnchor.MiddleCenter;
        style2.fontSize = 14;
        style2.normal.textColor = Color.red;
        labelRect2.size = labelSize;
        labelRect2.position = (centerPosition + outputPosition) - labelRect2.center;
        labelRect2.y += 30;
    }

    void LateUpdate()
    {
        
        if (outsideBoundary)
        {
            // translate ship position to boundary space, so we have no negative values
            shipPosition = gameObject.transform.position + boundary.position + (boundaryScaleForAction/2);

            // calculate the difference for every axis and set autopilot direction
            for (int i = 0; i < 3; i++)
			{
                if (shipPosition[i] < warningDistance[i]) // 0 <-> warning boundary min
                {
                    boundaryDiff[i] = shipPosition[i];
                    autopilotDirection[i] = 1.0f;
                }
                else if (shipPosition[i] > boundaryScaleForAction[i]-warningDistance[i]) // warning boundary max <-> final boundary max
                {
                    boundaryDiff[i] = boundaryScaleForAction[i] - shipPosition[i];
                    autopilotDirection[i] = -1.0f;
                }
                else // neither ... nor
                {
                    boundaryDiff[i] = warningDistance[i];
                    autopilotDirection[i] = 0.0f;
                }
			}

            // get the smallest difference value for text output (axes independently)
            unitsToTriggerAction = Mathf.Min(boundaryDiff);
            
        }

    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("BoundaryForWarning") && !outsideBoundary)
        {
            outsideBoundary = true;
            alpha = 0.0f;
            alphaAdd = 0.0f;
        }

        if (other.gameObject.CompareTag("Boundary") && boundaryAction == EBoundaryAction.Autopilot)
        {
            Text1 = "AUTOPILOT ACTIVE!";
            shipControl.StartCourseCorrection(autopilotDirection);
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("BoundaryForWarning") && outsideBoundary)
        {
            outsideBoundary = false;
        }
        if (other.gameObject.CompareTag("Boundary") && outsideBoundary)
        {
            Text1 = "WARNING!";
            shipControl.StopCourseCorrection();
        }
    }

    // ***************
    // * GUI Stuff *
    // ***************
    void OnGUI()
    {
        if (useGUIOutput && outsideBoundary)
        {
            // calculate alpha for text color
            if (alpha < 0.1f)
            {
                alphaAdd = 2f;
            }
            if (alpha > 0.9f)
            {
                alphaAdd = -0.5f;
            }
            alpha += alphaAdd * Time.deltaTime;
            textColor1.a = alpha;
            style1.normal.textColor = textColor1;

            // set GUI labels
            GUI.Label(labelRect1, Text1, style1);
            
            Text2 = "Environment boundary reached in ";

            if (unitsToTriggerAction > 0.0f)
            {
                Text2 += (int)unitsToTriggerAction;   
            }
            else
            {
                Text2 += "0";
            }
            Text2 += " Units!";
            
            GUI.Label(labelRect2, Text2, style2);
        }
        
        // GUI.Label(new Rect(20, 700, 400, 100), "Ship position in boundary space: " + shipPosition.ToString());
        // Debug.Log(cam.pixelRect.ToString());
    }

}
