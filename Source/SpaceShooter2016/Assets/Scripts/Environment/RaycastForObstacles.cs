﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RaycastForObstacles : MonoBehaviour
{ 
    public float maxSearchDistance = 1.0f;
	public float timerHealthbarFade = 1.0f;
	private float reset = 0.0f;
//    private bool enemyFound = false;
	private Transform lastRayTarget = null;

	private PlayerController playerControl;
   
	void Start () {

		playerControl = (GetComponent<Vehicle>().control) as PlayerController;
		reset = timerHealthbarFade;
	}

	// Update is called once per frame
	void Update ()
    {
		DamageCalc calcPlayer = transform.GetComponent<DamageCalc> ();
		float hitPointsPlayer = calcPlayer.leftHitpoints;
		float maxPointsPlayer = calcPlayer.maxhitpoints;
		float fillAmountPlayer = hitPointsPlayer / maxPointsPlayer;

		if (playerControl != null) {
			playerControl.playerInterface.healthBar.fillAmount = fillAmountPlayer;
		}


		timerHealthbarFade -= Time.deltaTime;
		if (playerControl != null && (timerHealthbarFade <= 0 || lastRayTarget == null)) {
			playerControl.playerInterface.healthBarEnemy.gameObject.SetActive (false);
		}



        Vector3 fwd = this.transform.TransformDirection(Vector3.forward);
		Debug.DrawRay (transform.position, fwd);
        // Sucht die Umgebung vor dem Raumschiff nach Collidern ab
		RaycastHit ray;
        if(Physics.Raycast(transform.position,fwd,out ray,maxSearchDistance))
        {
			//playerControl.playerInterface.healthBarEnemy.gameObject.SetActive (true);

			if (ray.collider.transform.GetComponent<DamageCalc> () != null) {
				DamageCalc calc = ray.collider.transform.GetComponent<DamageCalc> ();
				float hitPoints = calc.leftHitpoints;
				float maxPoints = calc.maxhitpoints;
				float fillAmount = hitPoints / maxPoints;
				playerControl.playerInterface.healthBarEnemy.fillAmount = fillAmount;
				playerControl.playerInterface.healthBarEnemy.gameObject.SetActive (true);
				timerHealthbarFade = reset;
				lastRayTarget = ray.transform;
			} /*else {
				playerControl.playerInterface.healthBarEnemy.gameObject.SetActive (false);
			}*/
//			Debug.Log("I found something in front of you!!!!!!!!!!!!!");
//			enemyFound = true;

//			Debug.Log (ray.distance + "");

		}
	}
}
